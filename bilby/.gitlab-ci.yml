# This script is an edited version of the example found at
# https://git.ligo.org/lscsoft/example-ci-project/blob/python/.gitlab-ci.yml
# Each 0th-indentation level is a job that will be run within GitLab CI
# The only exception are a short list of reserved keywords
#
# https://docs.gitlab.com/ee/ci/yaml/#gitlab-ci-yml

# stages is a reserved keyword that defines job dependencies and
# parallelization. each stage runs in parallel but must complete
# before the next stage begins

stages:
  - initial
  - test
  - docs
  - deploy

# ------------------- Initial stage -------------------------------------------

# Check author list is up to date
authors:
  stage: initial
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python37
  script:
    - python test/check_author_list.py

# Test containers scripts are up to date
containers:
  stage: initial
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python37
  script:
    - cd containers
    - python write_dockerfiles.py #HACK
    # Fail if differences exist. If this fails, you may need to run
    # write_dockerfiles.py and commit the changes.
    - git diff --exit-code

.test-python: &test-python
  stage: initial
  image: python
  before_script:
    # this is required because pytables doesn't use a wheel on py37
    - apt-get -yqq update
    - apt-get -yqq install libhdf5-dev
  script:
    - python -m pip install .
    - python -c "import bilby"
    - python -c "import bilby.core"
    - python -c "import bilby.core.prior"
    - python -c "import bilby.core.sampler"
    - python -c "import bilby.gw"
    - python -c "import bilby.gw.detector"
    - python -c "import bilby.gw.sampler"
    - python -c "import bilby.hyper"
    - python -c "import cli_bilby"
    - python test/import_test.py
    - for script in $(pip show -f bilby | grep "bin\/" | xargs -I {} basename {}); do
          ${script} --help;
      done

# Test basic setup on python 3.7
basic-3.7:
  <<: *test-python
  image: python:3.7

# Test basic setup on python 3.8
basic-3.8:
  <<: *test-python
  image: python:3.8

# Test basic setup on python 3.9
basic-3.9:
  <<: *test-python
  image: python:3.9


precommits-py3.7:
  stage: initial
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python37
  script:
    - source activate python37
    - mkdir -p .pip37
    - pip install --upgrade pip
    - pip --cache-dir=.pip37 install --upgrade bilby
    - pip --cache-dir=.pip37 install .
    - pip --cache-dir=.pip37 install pre-commit
    # Run precommits (flake8, spellcheck, isort, no merge conflicts, etc)
    - pre-commit run --all-files --verbose --show-diff-on-failure

precommits-py3.8:
  stage: initial
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python38
  script:
    - source activate python38
    - mkdir -p .pip38
    - pip install --upgrade pip
    - pip --cache-dir=.pip38 install --upgrade bilby
    - pip --cache-dir=.pip38 install .
    - pip --cache-dir=.pip38 install pre-commit
    # Run precommits (flake8, spellcheck, isort, no merge conflicts, etc)
    - pre-commit run --all-files --verbose --show-diff-on-failure

precommits-py3.9:
  stage: initial
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python39
  script:
    - source activate python39
    - mkdir -p .pip38
    - pip install --upgrade pip
    - pip --cache-dir=.pip39 install --upgrade bilby
    - pip --cache-dir=.pip39 install .
    - pip --cache-dir=.pip39 install pre-commit
    # Run precommits (flake8, spellcheck, isort, no merge conflicts, etc)
    - pre-commit run --all-files --verbose --show-diff-on-failure

# ------------------- Test stage -------------------------------------------

# test example on python 3.7 and build coverage
python-3.7:
  stage: test
  needs: ["basic-3.7", "precommits-py3.7"]
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python37
  script:
    - python -m pip install .

    # Run pyflakes
    - flake8 .

    # Run tests and collect coverage data
    - pytest --cov=bilby --durations 10
    - coverage html
    - coverage-badge -o coverage_badge.svg -f

  artifacts:
    paths:
      - coverage_badge.svg
      - htmlcov/

# test example on python 3.8
python-3.8:
  stage: test
  needs: ["basic-3.8", "precommits-py3.8"]
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python38
  script:
    - python -m pip install .
    - pytest

# test example on python 3.9
python-3.9:
  stage: test
  needs: ["basic-3.9", "precommits-py3.9"]
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python39
  script:
    - python -m pip install .
    - pytest


# test samplers on python 3.7
python-3.7-samplers:
  stage: test
  needs: ["basic-3.7", "precommits-py3.7"]
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python37
  script:
    - python -m pip install .

    - pytest test/integration/sampler_run_test.py --durations 10

# test samplers on python 3.8
python-3.8-samplers:
  stage: test
  needs: ["basic-3.8", "precommits-py3.8"]
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python38
  script:
    - python -m pip install .

    - pytest test/integration/sampler_run_test.py --durations 10

# test samplers on python 3.9
python-3.9-samplers:
  stage: test
  needs: ["basic-3.9", "precommits-py3.9"]
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python39
  script:
    - python -m pip install .

    - pytest test/integration/sampler_run_test.py --durations 10

integration-tests-python-3.7:
  stage: test
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python37
  needs: ["basic-3.7", "precommits-py3.7"]
  only:
    - schedules
  script:
    - python -m pip install .
    # Run tests which are only done on schedule
    - pytest test/integration/example_test.py

plotting-python-3.7:
  stage: test
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python37
  needs: ["basic-3.7", "precommits-py3.7"]
  only:
    - schedules
  script:
    - python -m pip install .
    - python -m pip install ligo.skymap
    - pytest test/gw/plot_test.py


# ------------------- Docs stage -------------------------------------------

docs:
  stage: docs
  needs: ["python-3.7"]
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python37
  script:
    # Make the documentation
    - apt-get -yqq install pandoc
    - python -m pip install .
    - cd docs
    - pip install ipykernel ipython jupyter
    - cp ../examples/tutorials/*.ipynb ./
    - rm basic_ptmcmc_tutorial.ipynb
    - rm compare_samplers.ipynb
    - rm visualising_the_results.ipynb
    - jupyter nbconvert --to notebook --execute *.ipynb --inplace
    - make clean
    - make html

  artifacts:
    paths:
      - docs/_build/html/

# ------------------- Deploy stage -------------------------------------------

deploy-docs:
  stage: deploy
  needs: ["docs", "python-3.7"]
  script:
    - mkdir public/
    - mv htmlcov/ public/
    - mv coverage_badge.svg public/
    - mv docs/_build/html/* public/
  artifacts:
    paths:
      - public
    expire_in: 30 days
  only:
    - master

# Build the containers
build-python37-container:
  stage: deploy
  image: docker:19.03.12
  needs: ["containers"]
  only:
    - schedules
  script:
    - cd containers
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --tag v3-bilby-python37 - < v3-dockerfile-test-suite-python37
    - docker image tag v3-bilby-python37 containers.ligo.org/lscsoft/bilby/v2-bilby-python37:latest
    - docker image push containers.ligo.org/lscsoft/bilby/v2-bilby-python37:latest

build-python38-container:
  stage: deploy
  image: docker:19.03.12
  needs: ["containers"]
  only:
    - schedules
  script:
    - cd containers
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --tag v3-bilby-python38 - < v3-dockerfile-test-suite-python38
    - docker image tag v3-bilby-python38 containers.ligo.org/lscsoft/bilby/v2-bilby-python38:latest
    - docker image push containers.ligo.org/lscsoft/bilby/v2-bilby-python38:latest

build-python39-container:
  stage: deploy
  image: docker:19.03.12
  needs: ["containers"]
  only:
    - schedules
  script:
    - cd containers
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --tag v3-bilby-python39 - < v3-dockerfile-test-suite-python39
    - docker image tag v3-bilby-python39 containers.ligo.org/lscsoft/bilby/v2-bilby-python39:latest
    - docker image push containers.ligo.org/lscsoft/bilby/v2-bilby-python39:latest

pypi-release:
  stage: deploy
  image: containers.ligo.org/lscsoft/bilby/v2-bilby-python37
  variables:
    TWINE_USERNAME: $PYPI_USERNAME
    TWINE_PASSWORD: $PYPI_PASSWORD
  before_script:
    - pip install twine
    - python setup.py sdist
  script:
    - twine upload dist/*
  only:
  - tags
