import numpy as np
import bilby

C = 299792458.
G = 6.67408*1e-11
Mo = 1.989*1e30
Mpc = 3.086*1e22

outdir = 'outdir_GW170817PeriastronEccFD'
label = 'GW170817PeriastronEccFD'
bilby.core.utils.setup_logger(outdir=outdir, label=label)

time_of_event = 1187008882.4
post_trigger_duration = 1.0
duration = 16.0
analysis_start = time_of_event + post_trigger_duration - duration
sampling_frequency = 512.

waveform_arguments = dict(waveform_approximant='PeriastronEccFD',
                          reference_frequency=10., minimum_frequency=10., catch_waveform_errors=True)

waveform_generator = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    frequency_domain_source_model=bilby.gw.source.lal_eccentric_advancement_of_pariastron,
    waveform_arguments=waveform_arguments)

ifos = bilby.gw.detector.InterferometerList(['H1', 'L1'])
ifos.set_strain_data_from_power_spectral_densities(
    sampling_frequency=sampling_frequency, duration=duration,
    start_time=analysis_start)

prior = bilby.core.prior.PriorDict()
prior['chirp_mass'] = bilby.gw.prior.Uniform(name='chirp_mass', minimum=0.8705505632961241,maximum=2.611651689888372)
prior['mass_ratio'] = bilby.gw.prior.Uniform(name='mass_ratio', minimum=0.3333333333333333, maximum=1)
prior['eccentricity'] = bilby.core.prior.analytical.LogUniform(name='eccentricity', latex_label='$e$', minimum=1e-4, maximum=0.2)
prior["luminosity_distance"] = 40.7
prior["theta_jn"] = bilby.core.prior.analytical.Sine(name='theta_jn')
prior["psi"] = bilby.gw.prior.Uniform(name='psi', minimum=0, maximum=6.283185307179586)
prior["phase"] = bilby.gw.prior.Uniform(name='phase', minimum=0.0, maximum=3.14)
prior["geocent_time"] = 1187008882.4
prior["ra"] = 3.44615914
prior["dec"] =  -0.40808407


    
likelihood = bilby.gw.likelihood.GravitationalWaveTransient(
    interferometers=ifos, waveform_generator=waveform_generator, priors=prior)

result_short = bilby.core.sampler.run_sampler(
    likelihood, prior, sampler='dynesty', outdir=outdir, label=label,
    nlive=500, dlogz=0.1, npool=8 )
