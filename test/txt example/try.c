#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int main()
{
    FILE *ptr = NULL;
    char *name[3] = {"Teemo", "Darius", "Luxian"};
    
    double age[3] = { 18., 17., 20 };
    double height[3] = { 1., 2., 1.5 };
    
    ptr = fopen("test.txt", "w");
    
    //column name
    char *colname[3] = {"Name", "Ages", "Height"};
    fprintf(ptr, "%s\t%s\t%s\n", colname[0], colname[1], colname[2]);
    
    int i;
    for (i=0;i<3;i++)
    {
        fprintf(ptr, "%s\t%.1f\t%.1f\n", name[i], age[i], height[i]); 
    }
       
    fclose(ptr);
    
    return 0;
}


