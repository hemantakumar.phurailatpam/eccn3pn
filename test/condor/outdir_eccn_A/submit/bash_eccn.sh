#!/usr/bin/env bash

# eccn_data0_1126259600-0_generation
# PARENTS 
# CHILDREN eccn_data0_1126259600-0_analysis_H1L1_dynesty
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_eccn/eccn_config_complete.ini --label eccn_data0_1126259600-0_generation --idx 0 --trigger-time 1126259600.0

# eccn_data0_1126259600-0_analysis_H1L1_dynesty
# PARENTS eccn_data0_1126259600-0_generation
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_eccn/eccn_config_complete.ini --outdir outdir_eccn --detectors H1 --detectors L1 --label eccn_data0_1126259600-0_analysis_H1L1_dynesty --data-dump-file outdir_eccn/data/eccn_data0_1126259600-0_generation_data_dump.pickle --sampler dynesty

