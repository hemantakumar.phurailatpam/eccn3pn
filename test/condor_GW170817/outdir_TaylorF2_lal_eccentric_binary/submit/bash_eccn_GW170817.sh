#!/usr/bin/env bash

# eccn_GW170817_data0_1187008882-4_generation
# PARENTS 
# CHILDREN eccn_GW170817_data0_1187008882-4_analysis_H1L1V1_dynesty
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_GW170817_local/eccn_GW170817_config_complete.ini --local --label eccn_GW170817_data0_1187008882-4_generation --idx 0 --trigger-time 1187008882.4

# eccn_GW170817_data0_1187008882-4_analysis_H1L1V1_dynesty
# PARENTS eccn_GW170817_data0_1187008882-4_generation
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_GW170817_local/eccn_GW170817_config_complete.ini --local --outdir outdir_GW170817_local --detectors H1 --detectors L1 --detectors V1 --label eccn_GW170817_data0_1187008882-4_analysis_H1L1V1_dynesty --data-dump-file outdir_GW170817_local/data/eccn_GW170817_data0_1187008882-4_generation_data_dump.pickle --sampler dynesty

