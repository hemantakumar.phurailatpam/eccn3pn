#!/usr/bin/env bash

# PeriastronEccFD_e0_0-0_data0_1187008882-4_generation
# PARENTS 
# CHILDREN PeriastronEccFD_e0_0-0_data0_1187008882-4_analysis_H1L1V1_dynesty
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_PeriastronEccFD_e0_0.0/PeriastronEccFD_e0_0.0_config_complete.ini --label PeriastronEccFD_e0_0-0_data0_1187008882-4_generation --idx 0 --trigger-time 1187008882.4

# PeriastronEccFD_e0_0-0_data0_1187008882-4_analysis_H1L1V1_dynesty
# PARENTS PeriastronEccFD_e0_0-0_data0_1187008882-4_generation
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_PeriastronEccFD_e0_0.0/PeriastronEccFD_e0_0.0_config_complete.ini --outdir outdir_PeriastronEccFD_e0_0.0 --detectors H1 --detectors L1 --detectors V1 --label PeriastronEccFD_e0_0-0_data0_1187008882-4_analysis_H1L1V1_dynesty --data-dump-file outdir_PeriastronEccFD_e0_0.0/data/PeriastronEccFD_e0_0-0_data0_1187008882-4_generation_data_dump.pickle --sampler dynesty

