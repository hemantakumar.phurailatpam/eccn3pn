#!/usr/bin/env bash

# logprior_data0_1187008882-4_generation
# PARENTS 
# CHILDREN logprior_data0_1187008882-4_analysis_H1L1V1_dynesty
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_logprior/logprior_config_complete.ini --label logprior_data0_1187008882-4_generation --idx 0 --trigger-time 1187008882.4

# logprior_data0_1187008882-4_analysis_H1L1V1_dynesty
# PARENTS logprior_data0_1187008882-4_generation
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_logprior/logprior_config_complete.ini --outdir outdir_logprior --detectors H1 --detectors L1 --detectors V1 --label logprior_data0_1187008882-4_analysis_H1L1V1_dynesty --data-dump-file outdir_logprior/data/logprior_data0_1187008882-4_generation_data_dump.pickle --sampler dynesty

