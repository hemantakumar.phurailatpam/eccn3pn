#!/usr/bin/env bash

# eccn_taylorf2_lal_taylorF2_fd_data0_1187008882-4_generation
# PARENTS 
# CHILDREN eccn_taylorf2_lal_taylorF2_fd_data0_1187008882-4_analysis_H1L1V1_dynesty
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_taylorf2_lal_taylorF2_fd/eccn_taylorf2_lal_taylorF2_fd_config_complete.ini --label eccn_taylorf2_lal_taylorF2_fd_data0_1187008882-4_generation --idx 0 --trigger-time 1187008882.4

# eccn_taylorf2_lal_taylorF2_fd_data0_1187008882-4_analysis_H1L1V1_dynesty
# PARENTS eccn_taylorf2_lal_taylorF2_fd_data0_1187008882-4_generation
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_taylorf2_lal_taylorF2_fd/eccn_taylorf2_lal_taylorF2_fd_config_complete.ini --outdir outdir_taylorf2_lal_taylorF2_fd --detectors H1 --detectors L1 --detectors V1 --label eccn_taylorf2_lal_taylorF2_fd_data0_1187008882-4_analysis_H1L1V1_dynesty --data-dump-file outdir_taylorf2_lal_taylorF2_fd/data/eccn_taylorf2_lal_taylorF2_fd_data0_1187008882-4_generation_data_dump.pickle --sampler dynesty

