#!/usr/bin/env bash

# eccnfd_GW170817_data0_1187008882-4_generation
# PARENTS 
# CHILDREN eccnfd_GW170817_data0_1187008882-4_analysis_H1L1V1_dynesty
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdirfd_GW170817/eccnfd_GW170817_config_complete.ini --label eccnfd_GW170817_data0_1187008882-4_generation --idx 0 --trigger-time 1187008882.4

# eccnfd_GW170817_data0_1187008882-4_analysis_H1L1V1_dynesty
# PARENTS eccnfd_GW170817_data0_1187008882-4_generation
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdirfd_GW170817/eccnfd_GW170817_config_complete.ini --outdir outdirfd_GW170817 --detectors H1 --detectors L1 --detectors V1 --label eccnfd_GW170817_data0_1187008882-4_analysis_H1L1V1_dynesty --data-dump-file outdirfd_GW170817/data/eccnfd_GW170817_data0_1187008882-4_generation_data_dump.pickle --sampler dynesty

