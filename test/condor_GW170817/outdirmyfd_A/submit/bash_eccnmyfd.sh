#!/usr/bin/env bash

# eccnmyfd_data0_1187008882-4_generation
# PARENTS 
# CHILDREN eccnmyfd_data0_1187008882-4_analysis_H1L1V1_dynesty
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdirmyfd_A/eccnmyfd_config_complete.ini --label eccnmyfd_data0_1187008882-4_generation --idx 0 --trigger-time 1187008882.4

# eccnmyfd_data0_1187008882-4_analysis_H1L1V1_dynesty
# PARENTS eccnmyfd_data0_1187008882-4_generation
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdirmyfd_A/eccnmyfd_config_complete.ini --outdir outdirmyfd_A --detectors H1 --detectors L1 --detectors V1 --label eccnmyfd_data0_1187008882-4_analysis_H1L1V1_dynesty --data-dump-file outdirmyfd_A/data/eccnmyfd_data0_1187008882-4_generation_data_dump.pickle --sampler dynesty

