import numpy as np
import bilby

C = 299792458.
G = 6.67408*1e-11
Mo = 1.989*1e30
Mpc = 3.086*1e22

outdir = 'outdir_OSMFD_recovery2'
label = 'OSMFD_recovery2'
bilby.core.utils.setup_logger(outdir=outdir, label=label)

time_of_event = 1126259642.413
post_trigger_duration = 1.0
duration = 16.0
analysis_start = time_of_event + post_trigger_duration - duration
sampling_frequency = 512.

# frequency_array, mass_1, mass_2, luminosity_distance, a_1, a_2, theta_jn, phase
'''injection_parameters = dict(
    mass_1=6.5, mass_2=5.8, a_1=0.0, a_2=0.0, luminosity_distance=200., theta_jn=0.4, psi=2.659,
    phase=1.3, geocent_time=1126259642.413, ra=1.375, dec=-1.2108)'''

injection_parameters = dict(
    mass_1=6.5, mass_2=5.8, eccentricity=0.0, luminosity_distance=200.,
    theta_jn=0.4, psi=2.659, phase=1.3, geocent_time=1126259642.413, ra=1.375, dec=-1.2108)

waveform_arguments = dict(waveform_approximant='OSMFD',
                          reference_frequency=10., minimum_frequency=10., catch_waveform_errors=True)

waveform_arguments2 = dict(waveform_approximant='TaylorF2',
                          reference_frequency=10., minimum_frequency=10., catch_waveform_errors=True)

waveform_generator = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    frequency_domain_source_model=bilby.gw.source.lal_eccentric_advancement_of_pariastron,
    waveform_arguments=waveform_arguments)

waveform_generator2 = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    frequency_domain_source_model=bilby.gw.source.lal_taylorF2_fd,
    waveform_arguments=waveform_arguments2)


ifos = bilby.gw.detector.InterferometerList(['H1', 'L1'])
ifos.set_strain_data_from_power_spectral_densities(
    sampling_frequency=sampling_frequency, duration=duration,
    start_time=analysis_start)

ifos.inject_signal(waveform_generator=waveform_generator,
                   parameters=injection_parameters)

prior = bilby.core.prior.PriorDict()
prior['chirp_mass'] = bilby.gw.prior.Uniform(name='chirp_mass', minimum=3.482202253184496, maximum=6.964404506368993, unit='Mo')
prior['mass_ratio'] = bilby.gw.prior.Uniform(name='mass_ratio', minimum=0.5, maximum=1.0)
prior['eccentricity'] = 0.0
prior["luminosity_distance"] = 200.0
prior["theta_jn"] = 0.4
prior["psi"] = 2.659
prior["phase"] = 1.3
prior["geocent_time"] = 1126259642.413
prior["ra"] = 1.375
prior["dec"] = -1.2108


    
likelihood = bilby.gw.likelihood.GravitationalWaveTransient(
    interferometers=ifos, waveform_generator=waveform_generator, priors=prior)

result_short = bilby.core.sampler.run_sampler(
    likelihood, prior, sampler='dynesty', outdir=outdir, label=label,
    nlive=500, dlogz=3.0, npool=8 )
