""" make sure that __init__.py is not present in the same folder while running following command in the terminal """
from distutils.core import setup, Extension
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("hphc.pyx")
)