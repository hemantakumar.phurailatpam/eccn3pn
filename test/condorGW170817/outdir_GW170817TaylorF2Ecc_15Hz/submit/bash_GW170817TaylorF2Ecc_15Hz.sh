#!/usr/bin/env bash

# GW170817TaylorF2Ecc_15Hz_data0_1187008882-4_generation
# PARENTS 
# CHILDREN GW170817TaylorF2Ecc_15Hz_data0_1187008882-4_analysis_H1L1V1_dynesty
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_GW170817TaylorF2Ecc_15Hz/GW170817TaylorF2Ecc_15Hz_config_complete.ini --label GW170817TaylorF2Ecc_15Hz_data0_1187008882-4_generation --idx 0 --trigger-time 1187008882.4

# GW170817TaylorF2Ecc_15Hz_data0_1187008882-4_analysis_H1L1V1_dynesty
# PARENTS GW170817TaylorF2Ecc_15Hz_data0_1187008882-4_generation
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_GW170817TaylorF2Ecc_15Hz/GW170817TaylorF2Ecc_15Hz_config_complete.ini --outdir outdir_GW170817TaylorF2Ecc_15Hz --detectors H1 --detectors L1 --detectors V1 --label GW170817TaylorF2Ecc_15Hz_data0_1187008882-4_analysis_H1L1V1_dynesty --data-dump-file outdir_GW170817TaylorF2Ecc_15Hz/data/GW170817TaylorF2Ecc_15Hz_data0_1187008882-4_generation_data_dump.pickle --sampler dynesty

