#!/usr/bin/env bash

# PeriastronEccFD15Hz_data0_1187008882-4_generation
# PARENTS 
# CHILDREN PeriastronEccFD15Hz_data0_1187008882-4_analysis_H1L1V1_dynesty
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_PeriastronEccFD15Hz/PeriastronEccFD15Hz_config_complete.ini --label PeriastronEccFD15Hz_data0_1187008882-4_generation --idx 0 --trigger-time 1187008882.4

# PeriastronEccFD15Hz_data0_1187008882-4_analysis_H1L1V1_dynesty
# PARENTS PeriastronEccFD15Hz_data0_1187008882-4_generation
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_PeriastronEccFD15Hz/PeriastronEccFD15Hz_config_complete.ini --outdir outdir_PeriastronEccFD15Hz --detectors H1 --detectors L1 --detectors V1 --label PeriastronEccFD15Hz_data0_1187008882-4_analysis_H1L1V1_dynesty --data-dump-file outdir_PeriastronEccFD15Hz/data/PeriastronEccFD15Hz_data0_1187008882-4_generation_data_dump.pickle --sampler dynesty

