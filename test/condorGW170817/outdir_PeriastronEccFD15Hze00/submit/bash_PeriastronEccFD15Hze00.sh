#!/usr/bin/env bash

# PeriastronEccFD15Hze00_data0_1187008882-4_generation
# PARENTS 
# CHILDREN PeriastronEccFD15Hze00_data0_1187008882-4_analysis_H1L1V1_dynesty
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_generation outdir_PeriastronEccFD15Hze00/PeriastronEccFD15Hze00_config_complete.ini --label PeriastronEccFD15Hze00_data0_1187008882-4_generation --idx 0 --trigger-time 1187008882.4

# PeriastronEccFD15Hze00_data0_1187008882-4_analysis_H1L1V1_dynesty
# PARENTS PeriastronEccFD15Hze00_data0_1187008882-4_generation
# CHILDREN 
/home/hemantakumar.phurailatpam/anaconda3/envs/lal2/bin/bilby_pipe_analysis outdir_PeriastronEccFD15Hze00/PeriastronEccFD15Hze00_config_complete.ini --outdir outdir_PeriastronEccFD15Hze00 --detectors H1 --detectors L1 --detectors V1 --label PeriastronEccFD15Hze00_data0_1187008882-4_analysis_H1L1V1_dynesty --data-dump-file outdir_PeriastronEccFD15Hze00/data/PeriastronEccFD15Hze00_data0_1187008882-4_generation_data_dump.pickle --sampler dynesty

