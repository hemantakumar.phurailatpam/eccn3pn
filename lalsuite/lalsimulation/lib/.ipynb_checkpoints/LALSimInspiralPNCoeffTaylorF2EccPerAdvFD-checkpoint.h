/*
 *  Copyright (C) 2022 Phurailatpam Hemantakumar
 *  Assembled from code found in:
 *    - LALSimInspiralTaylorF2Ecc.c
 *    - LALSimInspiralEccentricityFD.c
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA  02110-1301  USA
 */

#ifndef CORE_H2
#define CORE_H2

// #include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>


// Harmonics' variable. This struct is use in the function 'harmonics'.
typedef struct
{
    REAL8 Cp, Sp, Cx, Sx;
} CS_; /* Cp,Cx,Sp,Sx needed for calculating xi */
// sine cosine values needed for calculatng harmonics.
// s2b stands for sin(2*beta), and ci_2 stands for pow(cos(iota), 2) .
typedef struct
{
    REAL8 si, ci, s2b, c2b, si_2, ci_2;
} sincos_;
// powers of eccentricity value. Use inside the function 'harmonics'.
typedef struct
{
    REAL8 p1, p2, p3, p4, p5, p6;
} eccn;

// xi is the harmonics dependent amplitude. p and c stands for plus and cross polarization respectively.
typedef struct
{
    COMPLEX16 xi_p, xi_c;
} xi_;

// chi power values. p stands for power.
// these values are use for calculating the PN coefficients of k (periastron advancement), et (eccentricity at f) and psi (fourier phase).
typedef struct
{
    REAL8 p1, p2b3, p4b3, p7, p19b3, p19b9, p22b3, p23b3, p25b9, p28b9, p31b9, p38b9, p44b9, p47b9, p50b9, p9b2, p19b6, p19b18, p23b6, p25b6, p29b6, p31b6, p31b18, p37b18, p43b18, p49b18, p55b18, p95b18, p107b18, p113b18, p119b18, p125b18, p131b18, p8, p25b3, p34b9, p37b9, p53b9, p56b9;
} chi_struct;

////////////////////////////////////////////////////////////////////
// early declaration of functions
////////////////////////////////////////////////////////////////////
// // function to calculate the harmonics for the given j and n values
// void harmonics( int, CS_*, sincos_*, eccn* );
// void Xi_PlusCross( CS_*, eccn*, xi_* ); /*function for calculating xi (harmonics dependent amplitude) */
// // function to calculate PN coefficients for k (periastron advancement), et (eccentricity at f) and psi (fourier phase).
// void k_et_psi_PNe0Coeff1( REAL8*, REAL8*, REAL8*, chi_struct*, REAL8, REAL8 ); /* Harmonic indices independent */
// void et_psi_PNe0Coeff2( REAL8*, REAL8*, chi_struct*, REAL8, REAL8, REAL8, REAL8, REAL8 ); /* Harmonic indices dependent */
// // for ref phasing clasulation
// void k_psi_PNe0Coeff1( REAL8*, REAL8*, chi_struct*, REAL8, REAL8 ); /* Harmonic indices independent */
// void psi_PNe0Coeff2( REAL8*, chi_struct*, REAL8, REAL8, REAL8, REAL8, REAL8 ); /* Harmonic indices dependent */


// function to calculate the harmonics for the given j and n values
void harmonics( int cc , CS_* CS, sincos_* sc, eccn* et);
void Xi_PlusCross( CS_ *CS, eccn *et , xi_ *xi); /*function for calculating xi (harmonics dependent amplitude) */
// function to calculate PN coefficients for k (periastron advancement), et (eccentricity at f) and psi (fourier phase).
void k_et_psi_PNe0Coeff1(REAL8 *ke, REAL8 *ee, REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ln_chi); /* Harmonic indices independent */
void et_psi_PNe0Coeff2( REAL8 *ee, REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ll, REAL8 nn, REAL8 ln_x, REAL8 ln_v3 ); /* Harmonic indices dependent */
// for ref phasing clasulation
void k_psi_PNe0Coeff1(REAL8 *ke, REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ln_chi); /* Harmonic indices independent */
void psi_PNe0Coeff2( REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ll, REAL8 nn, REAL8 ln_x, REAL8 ln_v3 ); /* Harmonic indices dependent */


///////////////////////////////////////////////////////////////////////////////////////
////////////////////////////// VARIOUS FUNCTIONS //////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////
// 0PN harmoics
////////////////////////////////////////////////////////////////////
void harmonics( int cc , CS_* CS, sincos_* sc, eccn* et)
{
    switch( cc )
    {
        ////////////////////////////////////////////////////////////
        // l = 1, n = 0
        case 0:
        {
            CS->Cp = et->p1*sc->si_2-0.125*et->p3*sc->si_2+0.005208333333333333*et->p5*sc->si_2;
            CS->Sp = 0.;
            CS->Cx = 0.;
            CS->Sx = 0.;
            break;
        }
        // l = 2, n = 0
        case 1:
        {
            CS->Cp = et->p2*sc->si_2-0.3333333333333333*et->p4*sc->si_2+0.041666666666666664*et->p6*sc->si_2;
            CS->Sp = 0.;
            CS->Cx = 0.;
            CS->Sx = 0.;
            break;
        }
        // l = 3, n = 0
        case 2:
        {
            CS->Cp = 1.125*et->p3*sc->si_2-0.6328125*et->p5*sc->si_2;
            CS->Sp = 0.;
            CS->Cx = 0.;
            CS->Sx = 0.;
            break;
        }
        // l = 4, n = 0
        case 3:
        {
            CS->Cp = 1.3333333333333333*et->p4*sc->si_2-1.0666666666666667*et->p6*sc->si_2;
            CS->Sp = 0.;
            CS->Cx = 0.;
            CS->Sx = 0.;
            break;
        }
        // l = 5, n = 0
        case 4:
        {
            CS->Cp = 1.6276041666666667*et->p5*sc->si_2;
            CS->Sp = 0.;
            CS->Cx = 0.;
            CS->Sx = 0.;
            break;
        }
        // l = 6, n = 0
        case 5:
        {
            CS->Cp = 2.025*et->p6*sc->si_2;
            CS->Sp = 0.;
            CS->Cx = 0.;
            CS->Sx = 0.;
            break;
        }
        ////////////////////////////////////////////////////////////
        // l = 1, n = -2
        case 6:
        {           
            CS->Cp = et->p3*(-0.8125*sc->c2b-0.8125*sc->c2b*sc->ci_2)+et->p5*(-0.0130208333333333*sc->c2b-0.0130208333333333*sc->c2b*sc->ci_2)+et->p1*(1.5*sc->c2b+1.5*sc->c2b*sc->ci_2);
            CS->Sp = et->p3*(-0.8125*sc->s2b-0.8125*sc->ci_2*sc->s2b)+et->p5*(-0.0130208333333333*sc->s2b-0.0130208333333333*sc->ci_2*sc->s2b)+et->p1*(1.5*sc->s2b+1.5*sc->ci_2*sc->s2b);
            CS->Cx = -3.*et->p1*sc->ci*sc->s2b+1.625*et->p3*sc->ci*sc->s2b+0.0260416666666667*et->p5*sc->ci*sc->s2b;
            CS->Sx = 3.*et->p1*sc->c2b*sc->ci-1.625*et->p3*sc->c2b*sc->ci-0.0260416666666667*et->p5*sc->c2b*sc->ci;
            break;
        }
        // l = 2, n = -2
        case 7:
        {
            CS->Cp = -2.*sc->c2b-2.*sc->c2b*sc->ci_2+et->p4*(-2.875*sc->c2b-2.875*sc->c2b*sc->ci_2)+et->p6*(0.451388888888889*sc->c2b+0.451388888888889*sc->c2b*sc->ci_2)+et->p2*(5.*sc->c2b+5.*sc->c2b*sc->ci_2);
            CS->Sp = -2.*sc->s2b-2.*sc->ci_2*sc->s2b+et->p4*(-2.875*sc->s2b-2.875*sc->ci_2*sc->s2b)+et->p6*(0.451388888888889*sc->s2b+0.451388888888889*sc->ci_2*sc->s2b)+et->p2*(5.*sc->s2b+5.*sc->ci_2*sc->s2b);
            CS->Cx = 4.*sc->ci*sc->s2b-10.*et->p2*sc->ci*sc->s2b+5.75*et->p4*sc->ci*sc->s2b-0.902777777777778*et->p6*sc->ci*sc->s2b;
            CS->Sx = -4.*sc->c2b*sc->ci+10.*et->p2*sc->c2b*sc->ci-5.75*et->p4*sc->c2b*sc->ci+0.902777777777778*et->p6*sc->c2b*sc->ci;
            break;
        }
        // l = 3, n = -2
        case 8:
        {
            CS->Cp = et->p5*(-7.5234375*sc->c2b-7.5234375*sc->c2b*sc->ci_2)+et->p1*(-4.5*sc->c2b-4.5*sc->c2b*sc->ci_2)+et->p3*(10.6875*sc->c2b+10.6875*sc->c2b*sc->ci_2);
            CS->Sp = et->p5*(-7.5234375*sc->s2b-7.5234375*sc->ci_2*sc->s2b)+et->p1*(-4.5*sc->s2b-4.5*sc->ci_2*sc->s2b)+et->p3*(10.6875*sc->s2b+10.6875*sc->ci_2*sc->s2b);
            CS->Cx = 9.*et->p1*sc->ci*sc->s2b-21.375*et->p3*sc->ci*sc->s2b+15.046875*et->p5*sc->ci*sc->s2b;
            CS->Sx = -9.*et->p1*sc->c2b*sc->ci+21.375*et->p3*sc->c2b*sc->ci-15.046875*et->p5*sc->c2b*sc->ci;
            break;
        }
        // l = 4, n = -2
        case 9:
        {
            CS->Cp = et->p6*(-16.8333333333333*sc->c2b-16.8333333333333*sc->c2b*sc->ci_2)+et->p2*(-8.*sc->c2b-8.*sc->c2b*sc->ci_2)+et->p4*(20.*sc->c2b+20.*sc->c2b*sc->ci_2);
            CS->Sp = et->p6*(-16.8333333333333*sc->s2b-16.8333333333333*sc->ci_2*sc->s2b)+et->p2*(-8.*sc->s2b-8.*sc->ci_2*sc->s2b)+et->p4*(20.*sc->s2b+20.*sc->ci_2*sc->s2b);
            CS->Cx = 16.*et->p2*sc->ci*sc->s2b-40.*et->p4*sc->ci*sc->s2b+33.6666666666667*et->p6*sc->ci*sc->s2b;
            CS->Sx = -16.*et->p2*sc->c2b*sc->ci+40.*et->p4*sc->c2b*sc->ci-33.6666666666667*et->p6*sc->c2b*sc->ci;
            break;
        }
        // l = 5, n = -2
        case 10:
        {
            CS->Cp = et->p3*(-13.0208333333333*sc->c2b-13.0208333333333*sc->c2b*sc->ci_2)+et->p5*(34.9934895833333*sc->c2b+34.9934895833333*sc->c2b*sc->ci_2);
            CS->Sp = et->p3*(-13.0208333333333*sc->s2b-13.0208333333333*sc->ci_2*sc->s2b)+et->p5*(34.9934895833333*sc->s2b+34.9934895833333*sc->ci_2*sc->s2b);
            CS->Cx = 26.0416666666667*et->p3*sc->ci*sc->s2b-69.9869791666667*et->p5*sc->ci*sc->s2b;
            CS->Sx = -26.0416666666667*et->p3*sc->c2b*sc->ci+69.9869791666667*et->p5*sc->c2b*sc->ci;
            break;
        }
        // l = 6, n = -2
        case 11:
        {
            CS->Cp = et->p4*(-20.25*sc->c2b-20.25*sc->c2b*sc->ci_2)+et->p6*(58.725*sc->c2b+58.725*sc->c2b*sc->ci_2);
            CS->Sp = et->p4*(-20.25*sc->s2b-20.25*sc->ci_2*sc->s2b)+et->p6*(58.725*sc->s2b+58.725*sc->ci_2*sc->s2b);
            CS->Cx = 40.5*et->p4*sc->ci*sc->s2b-117.45*et->p6*sc->ci*sc->s2b;
            CS->Sx = -40.5*et->p4*sc->c2b*sc->ci+117.45*et->p6*sc->c2b*sc->ci;
            break;
        }
        // l = 7, n = -2
        case 12:
        {
            CS->Cp = et->p5*(-30.6377604166667*sc->c2b-30.6377604166667*sc->c2b*sc->ci_2);
            CS->Sp = et->p5*(-30.6377604166667*sc->s2b-30.6377604166667*sc->ci_2*sc->s2b);
            CS->Cx = 61.2755208333333*et->p5*sc->ci*sc->s2b;
            CS->Sx = -61.2755208333333*et->p5*sc->c2b*sc->ci;
            break;
        }
        // l = 8, n = -2
        case 13:
        {
            CS->Cp = et->p6*(-45.5111111111111*sc->c2b-45.5111111111111*sc->c2b*sc->ci_2);
            CS->Sp = et->p6*(-45.5111111111111*sc->s2b-45.5111111111111*sc->ci_2*sc->s2b);
            CS->Cx = 91.0222222222222*et->p6*sc->ci*sc->s2b;
            CS->Sx = -91.0222222222222*et->p6*sc->c2b*sc->ci;
            break;
        }
        ////////////////////////////////////////////////////////////
        // l = 1, n = 2
        case 14:
        {
            CS->Cp = et->p5*(0.0611979166666667*sc->c2b+0.0611979166666667*sc->c2b*sc->ci_2)+et->p3*(0.145833333333333*sc->c2b+0.145833333333333*sc->c2b*sc->ci_2);
            CS->Sp = et->p3*(-0.145833333333333*sc->s2b-0.145833333333333*sc->ci_2*sc->s2b)+et->p5*(-0.0611979166666667*sc->s2b-0.0611979166666667*sc->ci_2*sc->s2b);
            CS->Cx = -0.291666666666667*et->p3*sc->ci*sc->s2b-0.122395833333333*et->p5*sc->ci*sc->s2b;
            CS->Sx = -0.291666666666667*et->p3*sc->c2b*sc->ci-0.122395833333333*et->p5*sc->c2b*sc->ci;
            break;
        }
        // l = 2, n = 2
        case 15:
        {
            CS->Cp = et->p6*(0.0458333333333333*sc->c2b+0.0458333333333333*sc->c2b*sc->ci_2)+et->p4*(0.125*sc->c2b+0.125*sc->c2b*sc->ci_2);
            CS->Sp = et->p4*(-0.125*sc->s2b-0.125*sc->ci_2*sc->s2b)+et->p6*(-0.0458333333333333*sc->s2b-0.0458333333333333*sc->ci_2*sc->s2b);
            CS->Cx = -0.25*et->p4*sc->ci*sc->s2b-0.0916666666666667*et->p6*sc->ci*sc->s2b;
            CS->Sx = -0.25*et->p4*sc->c2b*sc->ci-0.0916666666666667*et->p6*sc->c2b*sc->ci;
            break;
        }
        // l = 3, n = 2
        case 16:
        {
            CS->Cp = et->p5*(0.11953125*sc->c2b+0.11953125*sc->c2b*sc->ci_2);
            CS->Sp = et->p5*(-0.11953125*sc->s2b-0.11953125*sc->ci_2*sc->s2b);
            CS->Cx = -0.2390625*et->p5*sc->ci*sc->s2b;
            CS->Sx = -0.2390625*et->p5*sc->c2b*sc->ci;
            break;
        }
        // l = 4, n = 2
        case 17:
        {
            CS->Cp = et->p6*(0.122222222222222*sc->c2b+0.122222222222222*sc->c2b*sc->ci_2);
            CS->Sp = et->p6*(-0.122222222222222*sc->s2b-0.122222222222222*sc->ci_2*sc->s2b);
            CS->Cx = -0.244444444444445*et->p6*sc->ci*sc->s2b;
            CS->Sx = -0.244444444444445*et->p6*sc->c2b*sc->ci;
            break;
        }

        default:
        {
            CS->Cp = 0.;
            CS->Sp = 0.;
            CS->Cx = 0.;
            CS->Sx = 0.;
            break;
        }
    }
}

////////////////////////////////////////////////////////////////////
// function to find xi
// corresspond to function xi in Mathematica notebook. 
////////////////////////////////////////////////////////////////////
void Xi_PlusCross( CS_ *CS, eccn *et , xi_ *xi)
{
    REAL8 numerator, denominator, Gamma_l, Sigma_l, al, phil;

    //////////// plus /////////////
    // repetitive values for plus and cross
    numerator = pow(1. - et->p2 , 1.75);
    denominator = pow( 1. + 3.0416666666666665*et->p2 + 0.3854166666666667*et->p4 , 0.5);

    Gamma_l = CS->Cp;
    Sigma_l = CS->Sp;

    al = copysignf( 1.0, Gamma_l )*sqrt( pow(Gamma_l,2.) + pow(Sigma_l,2.) ); /* alpha_l */

    // to avoid division by 0.
    if(Gamma_l==0)
    {
        phil = -copysign(1.0,Sigma_l) * 1.5707963267948966; /*Pi_b2 = 1.5707963267948966*/
    }
    else
    {
        phil = atan(- (Sigma_l/Gamma_l));
    }
        
    xi->xi_p = (numerator/denominator)*al * ( cos(phil) - I*sin(phil) );

    //////////// cross /////////////
    Gamma_l = CS->Cx;
    Sigma_l = CS->Sx;

    al = copysignf( 1.0, Gamma_l )*sqrt( pow(Gamma_l,2.) + pow(Sigma_l,2.) );

    if(Gamma_l==0)
    {
        phil = -copysign(1.0,Sigma_l) * 1.5707963267948966; /*Pi_b2 = 1.5707963267948966*/
    }
    else
    {
        phil = atan(- (Sigma_l/Gamma_l));
    }
        
    xi->xi_c = (numerator/denominator)*al * ( cos(phil) - I*sin(phil) );

}

////////////////////////////////////////////////////////////////////
// function to calculate PN coefficients for k (periastron advancement), et (eccentricity at f) and psi (fourier phase).
// Harmonic indices independent
////////////////////////////////////////////////////////////////////
void k_et_psi_PNe0Coeff1(REAL8 *ke, REAL8 *ee, REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ln_chi)
{
    REAL8 eta_2 = eta*eta, eta_3 = eta_2*eta;
    //////////////////////////////////
    /////// k 1PN Coefficients //////
    ke[0] = 3.; /* k1PN_et0_0_Coeff */

    ke[1] = (3./chi->p19b9);  /* k1PN_et0_2_Coeff */
 
    ke[2] = 10.930921052631579/chi->p19b9 - 7.930921052631579/chi->p38b9;  /* k1PN_et0_4_Coeff */

    ke[3] = 36.415447541551245/chi->p19b3 + 24.37940039242844/chi->p19b9 - 57.79484793397968/chi->p38b9;  /* k1PN_et0_6_Coeff */

    /////// k 2PN Coefficients //////
    ke[4] = 13.5 - 7.*eta;  /* k2PN_et0_0_Coeff */

    ke[5] = 31.31845238095238/chi->p19b9 + 8.431547619047619/chi->p25b9 - (4.083333333333333*eta)/chi->p19b9 - (16.416666666666664*eta)/chi->p25b9;  /* k2PN_et0_2_Coeff */

    ke[6] = 114.11317682226398/chi->p19b9 + 63.912452907107514/chi->p25b9 - 67.44567071725788/chi->p38b9 - 44.57995901211361/chi->p44b9 - (14.878198099415204*eta)/chi->p19b9 - (97.63633040935672*eta)/chi->p25b9 - (8.284996345029239*eta)/chi->p38b9 + (86.79952485380116*eta)/chi->p44b9;  /* k2PN_et0_4_Coeff */

    ke[7] = 308.4999814162741/chi->p19b3 + 254.50836342214728/chi->p19b9 + 214.05502613533707/chi->p25b9 - 491.4955346347543/chi->p38b9 - 500.3564163545241/chi->p44b9 + 307.03858001551987/chi->p7 + (113.84228312738054*eta)/chi->p19b3 - (33.18307275636093*eta)/chi->p19b9 - (302.4607893424339*eta)/chi->p25b9 - (60.3750939792372*eta)/chi->p38b9 + (832.4969367577844*eta)/chi->p44b9 - (597.820263807133*eta)/chi->p7;  /* k2PN_et0_6_Coeff */

    /////// k 5b2PN Coefficients //////
    ke[8] = 0;  /* k5b2PN_et0_0_Coeff */

    ke[9] = (-49.349184600139665*(-1. + chi->p1))/chi->p28b9;  /* k5b2PN_et0_2_Coeff */

    ke[10] = (-0.00007176518305897736*(3.635788e6 - 2.258257e6*chi->p1 - 3.883073e6*chi->p19b9 + 2.505542e6*chi->p28b9))/chi->p47b9;  /* k5b2PN_et0_2_Coeff */

    ke[11] = (-5.426889221035796e-9*(-3.3114231189e11 + 1.89286331247e11*chi->p1 + 4.46685572185e11*chi->p19b9 - 2.17621452319e11*chi->p28b9 - 1.61105816843e11*chi->p38b9 + 7.389767762e10*chi->p47b9))/chi->p22b3; /* k5b2PN_et0_6_Coeff */

    /////// k 3PN Coefficients //////
    ke[12] = 0.03125*(2160. - 3978.038658666009*eta + 224.*eta_2);  /* k1PN_et0_0_Coeff */

    ke[13] = (9.841899722852105e-7*(-1.193251e6 + 8.9434977e7*chi->p2b3 + 2.85923842e8*chi->p4b3 - 2.2282512e7*eta - 1.85795232e8*chi->p2b3*eta - 3.5172988134238017e8*chi->p4b3*eta + 4.270056e7*eta_2 + 2.2703856e7*chi->p2b3*eta_2 - 3.424512e6*chi->p4b3*eta_2))/chi->p31b9; /* k3PN_et0_2_Coeff */

    ke[14] = (1.1858853531487498e-11*(-4.759063292165e12 + 2.043730880707e12*chi->p19b9 + 5.626286182461e13*chi->p25b9 - 3.1968879219858e13*chi->p2b3 + 8.6461368353906e13*chi->p31b9 - 3.1736268138496e13*chi->p4b3 + 3.0348860970792e13*eta - 2.7141856655592e13*chi->p19b9*eta - 9.3285988260024e13*chi->p25b9*eta + 5.8318046249232e13*chi->p2b3*eta - 1.0636065400876634e14*chi->p31b9*eta + 2.9600980825294977e13*chi->p4b3*eta - 3.8763744107088e13*eta_2 + 3.5073241576464e13*chi->p19b9*eta_2 + 1.1206302010176e13*chi->p25b9*eta_2 + 7.646159215968e12*chi->p2b3*eta_2 - 1.035548457216e12*chi->p31b9*eta_2 + 2.19411602592e11*chi->p4b3*eta_2))/chi->p50b9; /* k3PN_et0_4 _Coeff */

    ke[15] = (2.5006017062009736e-16*(3.279894679024105e18 - 4.220015697065866e18*chi->p19b9 - 1.7016310487961012e19*chi->p25b9 + 1.0402025549836075e19*chi->p2b3 - 1.096780037851911e19*chi->p31b9 + 4.930394689428237e17*chi->p38b9 + 8.936345368795117e18*chi->p44b9 + 6.636901757399139e18*chi->p4b3 + 9.145059780731944e18*chi->p50b9 - 1.663185954089631e19*eta + 2.169227230953329e19*chi->p19b9*eta + 2.62215950342017e19*chi->p25b9*eta - 1.6414748179030198e19*chi->p2b3*eta + 1.0229862165375343e19*chi->p31b9*eta - 6.093524899450136e18*chi->p38b9*eta - 1.3792229587322483e19*chi->p44b9*eta - 3.280738282899852e18*chi->p4b3*eta - 1.1249816626155364e19*chi->p50b9*eta + 1.920250379150708e19*eta_2 - 2.3329096412641116e19*chi->p19b9*eta_2 + 3.477817715529865e18*chi->p25b9*eta_2 - 7.473844435281261e18*chi->p2b3*eta_2 + 7.582689456297446e16*chi->p31b9*eta_2 + 7.224047870706816e18*chi->p38b9*eta_2 + 1.6463347202736689e18*chi->p44b9*eta_2 + 6.231184695643471e17*chi->p4b3*eta_2 - 1.0953044957976576e17*chi->p50b9*eta_2))/chi->p23b3; /* k3PN_et0_6_Coeff */

    //////////////////////////////////
    /////// et 0PN Coefficients //////
    ee[0]= (1/chi->p19b18); /* et0PN_et0_1_Coeff */

    ee[1] = 1.8218201754385963/chi->p19b18 - 1.8218201754385963/chi->p19b6; /* et0PN_et0_3_Coeff */

    ee[2] = 2.403719022920514/chi->p19b18 - 9.957086254905356/chi->p19b6 + 7.553367231984841/chi->p95b18; /* et0PN_et0_5_Coeff */

    /////// et 1PN Coefficients //////
    ee[3] = (1.4052579365079365 - 2.736111111111111*eta)/chi->p31b18 + (-1.4052579365079365 + 2.736111111111111*eta)/chi->p19b18; /* et1PN_et0_1_Coeff */

    ee[4] = (8.09194822409255 - 11.288019310428849*eta)/chi->p31b18 + (2.148560817608926 - 8.6507903874269*eta)/chi->p19b6 + (-2.560127260425369 + 4.984702424463937*eta)/chi->p19b18 + (-7.680381781276107 + 14.954107273391813*eta)/chi->p23b6; /* et1PN_et0_3_Coeff */

    ee[5] = (53.07214625052842 - 103.33426004868151*eta)/chi->p107b18 + (11.742874337030363 - 47.2805533839138*eta)/chi->p19b6 + (17.555917921872364 - 23.268547909990602*eta)/chi->p31b18 + (-3.377845234094155 + 6.5768423266019616*eta)/chi->p19b18 + (-6.782320707168325 + 51.124906381868165*eta)/chi->p95b18 + (-72.21077256816866 + 116.18161263411578*eta)/chi->p23b6; /* et1PN_et0_5_Coeff */

    /////// et 3b2PN Coefficients //////
    ee[6] = ((377.*3.141592653589793)/(144.*chi->p37b18)-(377.*3.141592653589793)/(144.*chi->p19b18)); /* et3b2PN_et0_1_Coeff */

    ee[7] = -14.984223357663021/chi->p19b18 + 28.47620934225304/chi->p19b6 - 44.952670072989065/chi->p25b6 + 31.460684088399045/chi->p37b18; /* et3b2PN_et0_3_Coeff */

    ee[8] = 310.6270949032216/chi->p113b18 - 19.770262298161963/chi->p19b18 + 155.6355980991889/chi->p19b6 - 335.7384895736488/chi->p25b6 + 68.63126550839993/chi->p37b18 - 179.38520663899965/chi->p95b18; /* et3b2PN_et0_5_Coeff */

    /////// et 2PN Coefficients //////
    ee[9] = (-1.9747498681185438 + 7.689883708112874*eta - 7.486304012345679*eta_2)/chi->p31b18 + (-1.183105878829155 + 0.18990437610229277*eta + 3.2610918209876543*eta_2)/chi->p43b18 + (3.1578557469476984 - 7.879788084215167*eta + 4.2252121913580245*eta_2)/chi->p19b18; /* et2PN_et0_1_Coeff */

    ee[10] = (-4.326698974356313 + 40.990840987288095*eta - 58.739467687774116*eta_2)/chi->p9b2 + (-11.371274463717358 + 38.003048169910436*eta - 30.885275057701158*eta_2)/chi->p31b18 + (6.063565812754209 + 5.455687240099137*eta - 21.57641302420548*eta_2)/chi->p19b6 + (5.753045310914037 - 14.35555691000384*eta + 7.697576815725173*eta_2)/chi->p19b18 + (-5.176484108639348 - 15.988140562425905*eta + 32.49500785715978*eta_2)/chi->p43b18 + (9.057846423044774 - 54.10587892486793*eta + 71.0085710967958*eta_2)/chi->p23b6; /* et2PN_et0_3_Coeff */

    ee[11] = (-47.65455000845204 + 452.0043174617386*eta - 699.4171220297242*eta_2)/chi->p107b18 + (-92.10881407017493 + 575.5779173035754*eta - 654.6884059650216*eta_2)/chi->p9b2 + (33.14017959832605 + 29.81784325468656*eta - 117.92503368328093*eta_2)/chi->p19b6 + (-24.670592992393058 + 80.73325371331912*eta - 63.6653324759465*eta_2)/chi->p31b18 + (7.590597930577053 - 18.94079651461039*eta + 10.156222920242955*eta_2)/chi->p19b18 + (-24.587673108015718 - 53.21161753018305*eta + 106.66060870638786*eta_2)/chi->p43b18 + (-41.34875431268422 - 12.401253556161583*eta + 178.56934612868938*eta_2)/chi->p95b18 + (85.16166339627075 - 479.90659540433376*eta + 551.6805617376585*eta_2)/chi->p23b6 + (104.47794356654612 - 573.6730687280308*eta + 688.6291546609946*eta_2)/chi->p119b18; /* et2PN_et0_5_Coeff */

    /////// et 5b2PN Coefficients //////
    ee[12] = (1.6856863474839032 - 43.25439269500695*eta)/chi->p49b18 + (21.430424759029936 - 1.7538914078982104*eta)/chi->p19b18 + (-11.558055553256917 + 22.50414205145258*eta)/chi->p31b18 + (-11.558055553256917 + 22.50414205145258*eta)/chi->p37b18; /* et5b2PN_et0_1_Coeff */

    ee[13] = (231.97821220892166 - 312.1977300275552*eta)/chi->p49b18 + (120.04925753958759 - 233.74221835099368*eta)/chi->p23b6 + (53.014862206404956 - 213.4547258831219*eta)/chi->p25b6 + (39.042380194219554 - 3.195274752437365*eta)/chi->p19b18 + (-44.21037600319172 + 86.07992729742517*eta)/chi->p37b18 + (-66.55517444758593 + 92.84242478671577*eta)/chi->p31b18 + (-197.76591673185206 + 101.27142094401019*eta)/chi->p19b6 + (-135.55324496650408 + 482.39617598595714*eta)/chi->p29b6; /* et5b2PN_et0_3_Coeff */

    ee[14] = (1809.70780173107 - 5033.22254521683*eta)/chi->p125b18 + (1128.700353711886 - 1815.9925813167283*eta)/chi->p23b6 + (395.9526705139717 - 1594.2316027945642*eta)/chi->p25b6 + (948.4408704561301 - 1010.0350854528385*eta)/chi->p49b18 + (1269.1132319924927 - 1003.1442900261469*eta)/chi->p95b18 + (51.51271966254702 - 4.215862141301772*eta)/chi->p19b18 + (-96.44463054826241 + 187.7827681271498*eta)/chi->p37b18 + (-144.39503905856327 + 191.38064436455338*eta)/chi->p31b18 + (-1080.8818113485927 + 553.4949536133978*eta)/chi->p19b6 + (-278.9183304961662 + 2102.477035586392*eta)/chi->p113b18 + (-1260.4124266078522 + 2454.0892852696484*eta)/chi->p107b18 + (-2742.3754100086608 + 4971.617279987269*eta)/chi->p29b6; /* et5b2PN_et0_5_Coeff */

    ee[15] = - 7.935552146171627/chi->p19b18 + 4.437601850745452/chi->p31b18 - 67.64838946385173/chi->p37b18 + 1.6625689259538672/chi->p43b18 + 69.48377083332403/chi->p55b18 + (35.4145302358222*eta)/chi->p19b18 - (19.71337893985371*eta)/chi->p31b18 - (3.503973772380662*eta)/chi->p43b18 - (12.19717752358783*eta)/chi->p55b18 - (17.177287477571404*eta_2)/chi->p19b18 + (27.497488695758015*eta_2)/chi->p31b18 - (4.063075689621914*eta_2)/chi->p43b18 - (6.257125528564693*eta_2)/chi->p55b18 + (4.754564882687471*eta_3)/chi->p19b18 - (11.560650023576816*eta_3)/chi->p31b18 + (8.922709565757886*eta_3)/chi->p43b18 - (2.1166244248685415*eta_3)/chi->p55b18 + (4.387566137566138*ln_chi)/chi->p55b18; /* et3PN_et0_1_Coeff */

    ee[16] = - 14.457149003140529/chi->p19b18 + 359.32075915966425/chi->p19b6 + 25.562621945733145/chi->p23b6 + 702.6388557715335/chi->p25b6 + 25.55320520345388/chi->p31b18 - 736.372610288355/chi->p31b6 - 258.7598511208469/chi->p37b18 + 7.274295376872655/chi->p43b18 - 115.86281281556535/chi->p55b18 + 5.102685770648402/chi->p9b2 + (64.51890568730109*eta)/chi->p19b18 + (327.5376972217596*eta)/chi->p19b6 - (26.77182599992762*eta)/chi->p23b6 - (99.40877384538491*eta)/chi->p31b18 + (57.892152977211836*eta)/chi->p31b6 + (8.304025729215251*eta)/chi->p43b18 - (263.1846579548443*eta)/chi->p55b18 - (68.8875238153309*eta)/chi->p9b2 - (31.29392888594834*eta_2)/chi->p19b18 - (30.415699798629337*eta_2)/chi->p19b6 - (135.74337636006095*eta_2)/chi->p23b6 + (123.13739834498173*eta_2)/chi->p31b18 - (67.71275995506457*eta_2)/chi->p31b6 - (89.4091967270213*eta_2)/chi->p43b18 - (32.47898468853022*eta_2)/chi->p55b18 + (263.916548070273*eta_2)/chi->p9b2 + (8.66196222871188*eta_3)/chi->p19b18 - (40.301377143979074*eta_3)/chi->p19b6 + (177.1063902403533*eta_3)/chi->p23b6 - (47.69427680670878*eta_3)/chi->p31b18 + (146.41875884262853*eta_3)/chi->p31b6 + (88.90995205361773*eta_3)/chi->p43b18 - (54.18100372000784*eta_3)/chi->p55b18 - (278.92040569461574*eta_3)/chi->p9b2 - (23.980069531467556*ln_chi)/chi->p31b6 - (75.04505723220088*ln_chi)/chi->p55b18; /* et3PN_et0_3_Coeff */

    ee[17] = - 290.5283258130813/chi->p107b18 - 7377.0947308018285/chi->p113b18 - 93.81285171640539/chi->p119b18 + 7692.362026257606/chi->p131b18 - 19.07483765113045/chi->p19b18 + 1963.8534254729802/chi->p19b6 + 240.3391826262463/chi->p23b6 + 5247.806365438561/chi->p25b6 + 55.439056302526744/chi->p31b18 - 4122.596208248687/chi->p31b6 - 564.4828318192044/chi->p37b18 + 34.55202277530185/chi->p43b18 - 1476.5876312682922/chi->p55b18 - 1398.8030495457187/chi->p95b18 + 108.62838799112517/chi->p9b2 + (478.5391306154772*eta)/chi->p107b18 + (1222.270748057179*eta)/chi->p119b18 - (1155.6686900857303*eta)/chi->p131b18 + (85.12658001563956*eta)/chi->p19b18 + (1790.1443550458998*eta)/chi->p19b6 - (170.4424334121028*eta)/chi->p23b6 - (211.815630588921*eta)/chi->p31b18 + (2001.4600400591075*eta)/chi->p31b6 + (7.501442261504901*eta)/chi->p43b18 - (953.4793764541732*eta)/chi->p55b18 - (1977.4567360784695*eta)/chi->p95b18 - (1116.1794294354113*eta)/chi->p9b2 + (1424.3359925554669*eta_2)/chi->p107b18 - (4501.235629556758*eta_2)/chi->p119b18 + (2761.3715493466707*eta_2)/chi->p131b18 - (41.28937267201272*eta_2)/chi->p19b18 - (166.23580662967973*eta_2)/chi->p19b6 - (1203.137403384046*eta_2)/chi->p23b6 + (257.5287049921096*eta_2)/chi->p31b18 - (1826.6514838637872*eta_2)/chi->p31b6 - (295.4785648619477*eta_2)/chi->p43b18 - (156.58627090881123*eta_2)/chi->p55b18 + (242.17955180547295*eta_2)/chi->p95b18 + (3505.198733177323*eta_2)/chi->p9b2 - (2442.9278602327645*eta_3)/chi->p107b18 + (4660.980988027118*eta_3)/chi->p119b18 - (2975.0516201868027*eta_3)/chi->p131b18 + (11.428638054225717*eta_3)/chi->p19b18 - (220.26558593658302*eta_3)/chi->p19b6 + (1375.9768904790142*eta_3)/chi->p23b6 - (98.31455230449056*eta_3)/chi->p31b18 + (2352.073779988714*eta_3)/chi->p31b6 + (291.83527659942234*eta_3)/chi->p43b18 - (290.9734854822691*eta_3)/chi->p55b18 + (443.98132768585765*eta_3)/chi->p95b18 - (3108.7437966914413*eta_3)/chi->p9b2 + (165.70449145829178*ln_chi)/chi->p131b18 + (322.7810490360072*ln_chi)/chi->p31b6 - (450.6136623481717*ln_chi)/chi->p55b18; /* et3PN_et0_5_Coeff */

    //////////////////////////////////
    /////// psi 0PN Coefficients //////
    se[0] = 1.; /* psi0PN_et0_0_Coeff */

    se[1] = -1.6108071135430917/chi->p19b9; /* psi0PN_et0_2_Coeff */ 

    se[2] = -5.869201796385629/chi->p19b9 + 5.228286070090015/chi->p38b9; /* psi0PN_et0_4_Coeff */

    se[3] = -22.650035647987842/chi->p19b3 - 13.090170525346323/chi->p19b9 + 38.09998818181824/chi->p38b9; /* psi0PN_et0_6_Coeff */

    /////// psi 3b2PN Coefficients //////
    se[4] = -50.26548245743669; /* psi3b2PN_et0_0_Coeff */

    se[5] = 50.48185195147069/chi->p19b9 - 26.49733920048539/chi->p28b9; /* psi3b2PN_et0_2_Coeff */

    se[6] = 183.93771275738717/chi->p19b9 - 149.62757460393453/chi->p28b9 - 191.14321147088643/chi->p38b9 + 172.00776961014063/chi->p47b9; /* psi3b2PN_et0_4_Coeff */

    se[7] = 989.3998026374857/chi->p19b3 + 410.23909375873296/chi->p19b9 - 1117.760790392296/chi->p22b3 - 469.44472324492716/chi->p28b9 - 1392.9142362231482/chi->p38b9 + 1598.0435005233612/chi->p47b9; /* psi3b2PN_et0_6_Coeff */
    
    se[8] = - 3.4193121693121693 + 6.111111111111111*eta; /* psi1PN_et0_0_Coeff */

    se[9] = 4.6174595113029495/chi->p19b9 - 4.5271989609797405/chi->p25b9 - (10.325370012870012*eta)/chi->p19b9 + (8.814694482444141*eta)/chi->p25b9; /* psi1PN_et0_2_Coeff */

    se[10] = 16.824361793925114/chi->p19b9 - 34.31687792891888/chi->p25b9 - 18.56779383764494/chi->p38b9 + 29.388361977311536/chi->p44b9 - (37.62193481663054*eta)/chi->p19b9 + (52.42443185454516*eta)/chi->p25b9 + (42.454791781926005*eta)/chi->p38b9 - (57.22068643376294*eta)/chi->p44b9; /* psi1PN_et0_4_Coeff */

    se[11] = 98.90333052317057/chi->p19b3 + 37.523631407293855/chi->p19b9 - 114.93378626281779/chi->p25b9 - 135.30872570722394/chi->p38b9 + 329.84901303972686/chi->p44b9 - 190.9748541391356/chi->p7 - (226.81999104715237*eta)/chi->p19b3 - (83.90877658124401*eta)/chi->p19b9 + (162.40199701355033*eta)/chi->p25b9 + (309.37998484943006*eta)/chi->p38b9 - (548.8053794709137*eta)/chi->p44b9 + (371.8380852211337*eta)/chi->p7; /* psi1PN_et0_6_Coeff */

    /////// psi 2PN Coefficients //////
    se[12] = - 96.10716450932226 + 43.85912698412698*eta + 42.84722222222222*eta_2; /* psi2PN_et0_0_Coeff */

    se[13] = 56.34521097966795/chi->p19b9 + 12.977443249525058/chi->p25b9 + 0.6305695963516759/chi->p31b9 - (11.02099990468567*eta)/chi->p19b9 - (54.287380863896644*eta)/chi->p25b9 + (11.775120739510275*eta)/chi->p31b9 - (36.277272048441404*eta_2)/chi->p19b9 + (56.50271923709424*eta_2)/chi->p25b9 - (22.564971563560825*eta_2)/chi->p31b9; /* psi2PN_et0_2_Coeff */

    se[14] = 205.30168430420673/chi->p19b9 + 98.37105452220848/chi->p25b9 - 13.013337592397109/chi->p31b9 - 210.57745782683898/chi->p38b9 - 104.3701586151748/chi->p44b9 + 37.20487942148135/chi->p50b9 - (40.1565599597264*eta)/chi->p19b9 - (370.2505090644721*eta)/chi->p25b9 + (172.82419465202892*eta)/chi->p31b9 - (8.268968981387623*eta)/chi->p38b9 + (441.85392048937666*eta)/chi->p44b9 - (237.25797361353315*eta)/chi->p50b9 - (132.18133225545043*eta_2)/chi->p19b9 + (336.04374605848017*eta_2)/chi->p25b9 - (223.32682712918256*eta_2)/chi->p31b9 + (179.853147826764*eta_2)/chi->p38b9 - (464.6441100577457*eta_2)/chi->p44b9 + (303.0429176690523*eta_2)/chi->p50b9; /* psi2PN_et0_4_Coeff */

    se[15] = 1106.8518202223822/chi->p19b3 + 457.88748622305997/chi->p19b9 - 510.1379816462184/chi->p23b3 + 329.46347212360456/chi->p25b9 - 66.1985526505701/chi->p31b9 - 1534.5370446460222/chi->p38b9 - 1171.4294875159458/chi->p44b9 + 695.655631413319/chi->p50b9 + 833.9081409885185/chi->p7 + (279.06106249767885*eta)/chi->p19b3 - (89.56178980041588*eta)/chi->p19b9 + (2586.8340564340087*eta)/chi->p23b3 - (1202.2659321691117*eta)/chi->p25b9 + (818.1546393207492*eta)/chi->p31b9 - (60.25829808147164*eta)/chi->p38b9 + (4627.477530254056*eta)/chi->p44b9 - (3575.8993505095737*eta)/chi->p50b9 - (3536.1065648880704*eta)/chi->p7 - (1167.375190010506*eta_2)/chi->p19b3 - (294.80604680466854*eta_2)/chi->p19b9 - (2986.6588672499524*eta_2)/chi->p23b3 + (1041.0065214484537*eta_2)/chi->p25b9 - (969.9457009894719*eta_2)/chi->p31b9 + (1310.6403733077561*eta_2)/chi->p38b9 - (4456.416080124205*eta_2)/chi->p44b9 + (3845.7243906751146*eta_2)/chi->p50b9 + (3723.628186357419*eta_2)/chi->p7; /* psi2PN_et0_6_Coeff */

    /////// psi 5b2PN Coefficients //////
    se[16] = 60.06010399779534 - 22.689280275926283*eta; /* psi5b2PN_et0_0_Coeff */

    se[17] = - 210.61264554948588/chi->p19b9 + 141.88004620884573/chi->p25b9 + 75.95595393565333/chi->p28b9 - 42.66622732728404/chi->p34b9 + (216.02141641228727*eta)/chi->p19b9 - (276.2479120677702*eta)/chi->p25b9 - (169.84953027662291*eta)/chi->p28b9 + (211.84863109153514*eta)/chi->p34b9; /* psi5b2PN_et0_2_Coeff */

    se[18] = - 767.3967337291026/chi->p19b9 + 1075.4729951706481/chi->p25b9 + 428.91495927680893/chi->p28b9 - 1114.0818153683329/chi->p34b9 + 1381.167270248381/chi->p38b9 - 1074.422059716312/chi->p44b9 - 610.8703238075171/chi->p47b9 + 760.3988519534003/chi->p53b9 + (787.1043494934547*eta)/chi->p19b9 - (1642.9542589366743*eta)/chi->p25b9 - (959.1217091881848*eta)/chi->p28b9 + (1836.068203045879*eta)/chi->p34b9 - (1226.808302894472*eta)/chi->p38b9 + (2091.9562588758126*eta)/chi->p44b9 + (1396.7395712045059*eta)/chi->p47b9 - (2316.4824640732704*eta)/chi->p53b9; /* psi5b2PN_et0_4_Coeff */

    se[19] = - 8927.106385535517/chi->p19b3 - 1711.5366711865092/chi->p19b9 + 4880.7987155565725/chi->p22b3 + 3601.964712944092/chi->p25b9 + 1345.6868821558517/chi->p28b9 - 6026.136596175849/chi->p34b9 + 10064.953594375815/chi->p38b9 - 12059.095238419155/chi->p44b9 - 5675.30962604641/chi->p47b9 + 14635.606205092497/chi->p53b9 + 8342.171550214278/chi->p7 - 8082.796244251813/chi->p8 + (8387.957942353512*eta)/chi->p19b3 + (1755.4908680182211*eta)/chi->p19b9 - (11193.38160918795*eta)/chi->p22b3 - (5089.593596236571*eta)/chi->p25b9 - (3009.168774671892*eta)/chi->p28b9 + (7786.856910831304*eta)/chi->p34b9 - (8940.096470434935*eta)/chi->p38b9 + (20064.017404228016*eta)/chi->p44b9 + (12976.452161121204*eta)/chi->p47b9 - (27842.943893106512*eta)/chi->p53b9 - (16242.646759965388*eta)/chi->p7 + (21169.869809627333*eta)/chi->p8; /* psi5b2PN_et0_6_Coeff */

    /////// psi 3PN Coefficients //////
    se[20] = 229.55467539143297 - 4322.017571041981*eta + 63.80497685185184*eta_2 - 98.6304012345679*eta_3; /* psi3PN_et0_0_Coeff */

    se[21] = - 61.572441713118685/chi->p19b9 + 158.359109826785/chi->p25b9 + 830.4127436366826/chi->p28b9 - 1.8075594251680198/chi->p31b9 - 327.46225593097415/chi->p37b9 - (843.1840289315528*eta)/chi->p19b9 - (339.30821080736393*eta)/chi->p25b9 - (29.71198627806088*eta)/chi->p31b9 + (28.006148883848002*eta)/chi->p37b9 - (92.88746834092433*eta_2)/chi->p19b9 - (41.648488332329705*eta_2)/chi->p25b9 + (140.16285329998192*eta_2)/chi->p31b9 + (7.068382176074206*eta_2)/chi->p37b9 - (92.75010314857681*eta_3)/chi->p19b9 + (198.5172942650821*eta_3)/chi->p25b9 - (144.6428183515857*eta_3)/chi->p31b9 + (35.5644754417582*eta_3)/chi->p37b9 - (14.135045491064645*ln_chi)/chi->p37b9; /* psi3PN_et0_2_Coeff */

    se[22] = - 224.347833127953/chi->p19b9 + 1200.3868810930214/chi->p25b9 + 4689.249883183728/chi->p28b9 + 37.303385945224036/chi->p31b9 - 813.8958832352637/chi->p37b9 + 23.754151159535464/chi->p38b9 - 1183.6625754433232/chi->p44b9 - 6288.507751960206/chi->p47b9 - 132.12982640802795/chi->p50b9 + 3528.9609508334265/chi->p56b9 - (3072.259351030208*eta)/chi->p19b9 - (2068.572924531077*eta)/chi->p25b9 - (578.8255088297584*eta)/chi->p31b9 + (898.2444524268268*eta)/chi->p37b9 + (305.53635467707795*eta)/chi->p38b9 + (2258.1731512868505*eta)/chi->p44b9 + (1144.7121426163849*eta)/chi->p50b9 - (374.23363516095196*eta)/chi->p56b9 - (338.44852773781963*eta_2)/chi->p19b9 - (414.17315108632*eta_2)/chi->p25b9 + (1747.9910011211275*eta_2)/chi->p31b9 - (224.79069843736855*eta_2)/chi->p37b9 + (411.09532675080675*eta_2)/chi->p38b9 + (1101.4595251876121*eta_2)/chi->p44b9 - (3002.815086461273*eta_2)/chi->p50b9 + (784.0895319591057*eta_2)/chi->p56b9 - (337.9480183801763*eta_3)/chi->p19b9 + (1180.659906690582*eta_3)/chi->p25b9 - (1431.5383291528901*eta_3)/chi->p31b9 + (591.9984576931926*eta_3)/chi->p37b9 + (535.9959136507457*eta_3)/chi->p38b9 - (1968.3927845484725*eta_3)/chi->p44b9 + (2460.772765329089*eta_3)/chi->p50b9 - (1032.442463984194*eta_3)/chi->p56b9 + (216.01471299539128*ln_chi)/chi->p37b9 + (91.75780367454276*ln_chi)/chi->p56b9; /* psi3PN_et0_4_Coeff */

    se[23] = - 2127.154767957407/chi->p19b3 - 500.36640322119416/chi->p19b9 + 48826.07350369903/chi->p22b3 + 2227.561412940261/chi->p23b3 - 32553.810341481174/chi->p25b3 + 4020.325202951301/chi->p25b9 + 14712.151951032203/chi->p28b9 + 189.7614767161797/chi->p31b9 + 1799.2380407016244/chi->p37b9 + 173.1031673314792/chi->p38b9 - 13285.188626146024/chi->p44b9 - 58423.5756546798/chi->p47b9 - 2470.5592182443133/chi->p50b9 + 29156.777235788355/chi->p56b9 + 9332.47382943455/chi->p7 + (4500.5028725447555*eta)/chi->p19b3 - (6852.1070152744305*eta)/chi->p19b9 - (16404.21242140086*eta)/chi->p23b3 + (6619.517206182367*eta)/chi->p25b3 - (6467.105803737125*eta)/chi->p25b9 - (2769.61805915347*eta)/chi->p31b9 + (4864.168343934332*eta)/chi->p37b9 + (2226.529181122649*eta)/chi->p38b9 + (21582.317585649853*eta)/chi->p44b9 + (18348.36036551368*eta)/chi->p50b9 - (11603.982243956996*eta)/chi->p56b9 - (15817.900744912906*eta)/chi->p7 - (1707.7368030260425*eta_2)/chi->p19b3 - (754.8469273741383*eta_2)/chi->p19b9 + (38946.34870946779*eta_2)/chi->p23b3 - (16207.47774664614*eta_2)/chi->p25b3 - (1477.3040286445805*eta_2)/chi->p25b9 + (8024.8182867665155*eta_2)/chi->p31b9 - (1807.119638118998*eta_2)/chi->p37b9 + (2995.7670412125676*eta_2)/chi->p38b9 + (12214.794123021338*eta_2)/chi->p44b9 - (42694.80992604206*eta_2)/chi->p50b9 + (17114.129661563496*eta_2)/chi->p56b9 - (14424.031946538573*eta_2)/chi->p7 - (4164.277909396126*eta_3)/chi->p19b3 - (753.7306336993971*eta_3)/chi->p19b9 - (29908.736041689786*eta_3)/chi->p23b3 + (15629.616902071488*eta_3)/chi->p25b3 + (3657.484112987267*eta_3)/chi->p25b9 - (6217.409999562283*eta_3)/chi->p31b9 + (3638.1358466190395*eta_3)/chi->p37b9 + (3905.952677766288*eta_3)/chi->p38b9 - (18878.916287074204*eta_3)/chi->p44b9 + (31228.09771080014*eta_3)/chi->p50b9 - (17384.679703215024*eta_3)/chi->p56b9 + (19164.40936933914*eta_3)/chi->p7 - (596.2711765426641*ln_chi)/chi->p25b3 + (1858.1812925206564*ln_chi)/chi->p37b9 - (1067.9294554602477*ln_chi)/chi->p56b9; /* psi3PN_et0_6_Coeff */

}


////////////////////////////////////////////////////////////////////
// function to calculate PN coefficients for et (eccentricity at f) and psi (fourier phase).
// Harmonic indices dependent
////////////////////////////////////////////////////////////////////
void et_psi_PNe0Coeff2( REAL8 *ee, REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ll, REAL8 nn, REAL8 ln_x, REAL8 ln_v3 )
{
    REAL8 eta_2 = eta*eta;
    //////////////////////////////////
    /////// et 3PN Coefficients //////
    ee[15] = (6.581349206349206/chi->p19b18 - 6.581349206349205/chi->p55b18)*ln_x; /* et3PN_et0_1_Coeff */

    ee[16] = (11.990034765733778/chi->p19b18 - 160.52772491123642/chi->p19b6 + 35.97010429720133/chi->p31b6 + 112.56758584830132/chi->p55b18)*ln_x; /* et3PN_et0_3_Coeff */

    ee[17] = ( - 248.5567371874377/chi->p131b18 + 15.819714283784416/chi->p19b18 - 877.3579438816425/chi->p19b6 - 484.1715735540108/chi->p31b6 + 675.9204935222575/chi->p55b18 + 918.346046817049/chi->p95b18)*ln_x; /* et3PN_et0_5_Coeff */


    //////////////////////////////////
    /////// psi 1PN Coefficients //////
    se[8] = -(8.333333333333332*nn)/ll; /* psi1PN_et0_0_Coeff */

    se[9] = (10.494186046511627*nn)/(chi->p19b9*ll); /* psi1PN_et0_2_Coeff */

    se[10] = ((38.23703972868217/chi->p19b9 - 37.50080222848265/chi->p38b9)*nn)/ll; /* psi1PN_et0_4_Coeff */

    se[11] = ((184.58456450704332/chi->p19b3 + 85.28065447351422/chi->p19b9 - 273.2788723799295/chi->p38b9)*nn)/ll; /* psi1PN_et0_6_Coeff */

    /////// psi 2PN Coefficients //////
    se[12] = (( - 126.21031746031746 - 10.*eta)*nn)/ll; /* psi2PN_et0_0_Coeff */

    se[13] = ((64.19878487102324/chi->p19b9 + 29.494076458102615/chi->p25b9 + (28.998243751150728*eta)/chi->p19b9 - (57.42651808785529*eta)/chi->p25b9)*nn)/ll; /* psi2PN_et0_2_Coeff */

    se[14] = ((233.91728303334457/chi->p19b9 + 223.56972383203305/chi->p25b9 - 222.6873092462439/chi->p38b9 - 210.79319982795903/chi->p44b9 + (105.6591710362652*eta)/chi->p19b9 - (341.53793873815675*eta)/chi->p25b9 - (176.47747645649966*eta)/chi->p38b9 + (410.42544661172684*eta)/chi->p44b9)*nn)/ll; /* psi2PN_et0_4_Coeff */

    se[15] = ((1130.9419871413997/chi->p19b3 + 521.7092936926844/chi->p19b9 + 748.7777561517121/chi->p25b9 - 1622.7849311957646/chi->p38b9 - 2365.9001128547707/chi->p44b9 + 1556.3335453823029/chi->p7 + (1233.6668000381603*eta)/chi->p19b3 + (235.65326502884656*eta)/chi->p19b9 - (1058.0265983780876*eta)/chi->p25b9 - (1286.0409084757641*eta)/chi->p38b9 + (3936.4031962986637*eta)/chi->p44b9 - (3030.2632673239614*eta)/chi->p7)*nn)/ll; /* psi2PN_et0_6_Coeff */

    /////// psi 5b2PN Coefficients //////
    se[16] =   - 6.960539278786909*ln_v3 - 22.689280275926283*eta*ln_v3 + (( - 100.53096491487338 - 167.5516081914556*ln_v3)*nn)/ll; /* psi5b2PN_et0_0_Coeff */

    se[17] = (( - 331.6374815048508/chi->p19b9 + 172.62650814583742/chi->p28b9)*nn)/ll; /* psi5b2PN_et0_2_Coeff */

    se[18] = (( - 1208.3677094743632/chi->p19b9 + 974.8029993039772/chi->p28b9 + 1371.759871506524/chi->p38b9 - 1233.7560078844797/chi->p47b9)*nn)/ll; /* psi5b2PN_et0_4_Coeff */

    se[19] = (( - 8112.524009764331/chi->p19b3 - 2695.04098224778/chi->p19b9 + 9109.09774819447/chi->p22b3 + 3058.3675865754967/chi->p28b9 + 9996.399239070568/chi->p38b9 - 11462.24832808487/chi->p47b9)*nn)/ll; /* psi5b2PN_et0_6_Coeff */

    /////// psi 3PN Coefficients //////
    se[20] = -163.04761904761904*ln_x + ((507.8074314216428 - 1013.6970233660937*eta + 19.791666666666664*eta_2)*nn)/ll; /* psi3PN_et0_0_Coeff */

    se[21] = ( - 21.855386904761904/chi->p19b9 + 21.202568236596964/chi->p37b9)*ln_x + ((222.04259821895513/chi->p19b9 + 180.43170390834212/chi->p25b9 - 4.108073898949294/chi->p31b9 - (309.52963269393405*eta)/chi->p19b9 - (269.8099928586847*eta)/chi->p25b9 - (76.71328660124686*eta)/chi->p31b9 + (42.79468040085441*eta_2)/chi->p19b9 - (158.6848338604637*eta_2)/chi->p25b9 + (147.00767567470572*eta_2)/chi->p31b9)*nn)/ll; /* psi3PN_et0_2_Coeff */

    se[22] = ( - 79.63316961022348/chi->p19b9 - 324.0220694930869/chi->p37b9 + 552.5514750417711/chi->p38b9 - 137.6367055118141/chi->p56b9)*ln_x + ((809.0433704841975/chi->p19b9 + 1367.7006049209328/chi->p25b9 + 84.7800985186211/chi->p31b9 - 683.5025421320212/chi->p38b9 - 1251.732434711526/chi->p44b9 - 266.858547220905/chi->p50b9 - (1127.8146594758143*eta)/chi->p19b9 - (1471.5949611373971*eta)/chi->p25b9 - (1125.9257776852457*eta)/chi->p31b9 + (349.71794556861516*eta)/chi->p38b9 + (1445.2033869067816*eta)/chi->p44b9 + (1701.7745827856886*eta)/chi->p50b9 + (155.9284243114465*eta_2)/chi->p19b9 - (943.7607027261389*eta_2)/chi->p25b9 + (1454.9434586961715*eta_2)/chi->p31b9 - (428.7624994075837*eta_2)/chi->p38b9 + (1931.4479367739132*eta_2)/chi->p44b9 - (2173.6286748467505*eta_2)/chi->p50b9)*nn)/ll; /* psi3PN_et0_4_Coeff */

    se[23] = ( - 3566.310930959158/chi->p19b3 - 177.60707602754258/chi->p19b9 + 894.4067648139961/chi->p25b3 - 2787.2719387809843/chi->p37b9 + 4026.5977007978186/chi->p38b9 + 1601.8941831903715/chi->p56b9)*ln_x + ((3342.8217057556644/chi->p19b3 + 1804.421802051675/chi->p19b9 + 4157.326665798624/chi->p23b3 + 4580.690857807921/chi->p25b9 + 431.27443483710056/chi->p31b9 - 4980.874884878742/chi->p38b9 - 14049.19091775737/chi->p44b9 - 4989.712479966107/chi->p50b9 + 9535.591218963053/chi->p7 + (1186.2079528535864*eta)/chi->p19b3 - (2515.382282922241*eta)/chi->p19b9 - (21081.18703121239*eta)/chi->p23b3 - (4403.465107627784*eta)/chi->p25b9 - (5330.164566360338*eta)/chi->p31b9 + (2548.4928357993604*eta)/chi->p38b9 + (12241.310242655041*eta)/chi->p44b9 + (25648.767595096477*eta)/chi->p50b9 - (8164.577251678136*eta)/chi->p7 + (4215.308124947458*eta_2)/chi->p19b3 + (347.76954938614654*eta_2)/chi->p19b9 + (24339.525769859294*eta_2)/chi->p23b3 - (2923.6105648391167*eta_2)/chi->p25b9 + (6319.062385321006*eta_2)/chi->p31b9 - (3124.512687568861*eta_2)/chi->p38b9 + (18524.577105458782*eta_2)/chi->p44b9 - (27584.135195853287*eta_2)/chi->p50b9 - (20252.696633959793*eta_2)/chi->p7)*nn)/ll; /* psi3PN_et0_6_Coeff */

}


////////////////////////////////////////////////////////////////////
// for reference phasing
// function to calculate PN coefficients for k (periastron advancement) and psi (fourier phase).
// Harmonic indices independent
////////////////////////////////////////////////////////////////////
void k_psi_PNe0Coeff1(REAL8 *ke, REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ln_chi)
{
    REAL8 eta_2 = eta*eta, eta_3 = eta_2*eta;
    //////////////////////////////////
    /////// k 1PN Coefficients //////
    ke[0] = 3.; /* k1PN_et0_0_Coeff */

    ke[1] = (3./chi->p19b9);  /* k1PN_et0_2_Coeff */
 
    ke[2] = 10.930921052631579/chi->p19b9 - 7.930921052631579/chi->p38b9;  /* k1PN_et0_4_Coeff */

    ke[3] = 36.415447541551245/chi->p19b3 + 24.37940039242844/chi->p19b9 - 57.79484793397968/chi->p38b9;  /* k1PN_et0_6_Coeff */

    /////// k 2PN Coefficients //////
    ke[4] = 13.5 - 7.*eta;  /* k2PN_et0_0_Coeff */

    ke[5] = 31.31845238095238/chi->p19b9 + 8.431547619047619/chi->p25b9 - (4.083333333333333*eta)/chi->p19b9 - (16.416666666666664*eta)/chi->p25b9;  /* k2PN_et0_2_Coeff */

    ke[6] = 114.11317682226398/chi->p19b9 + 63.912452907107514/chi->p25b9 - 67.44567071725788/chi->p38b9 - 44.57995901211361/chi->p44b9 - (14.878198099415204*eta)/chi->p19b9 - (97.63633040935672*eta)/chi->p25b9 - (8.284996345029239*eta)/chi->p38b9 + (86.79952485380116*eta)/chi->p44b9;  /* k2PN_et0_4_Coeff */

    ke[7] = 308.4999814162741/chi->p19b3 + 254.50836342214728/chi->p19b9 + 214.05502613533707/chi->p25b9 - 491.4955346347543/chi->p38b9 - 500.3564163545241/chi->p44b9 + 307.03858001551987/chi->p7 + (113.84228312738054*eta)/chi->p19b3 - (33.18307275636093*eta)/chi->p19b9 - (302.4607893424339*eta)/chi->p25b9 - (60.3750939792372*eta)/chi->p38b9 + (832.4969367577844*eta)/chi->p44b9 - (597.820263807133*eta)/chi->p7;  /* k2PN_et0_6_Coeff */

    /////// k 5b2PN Coefficients //////
    ke[8] = 0;  /* k5b2PN_et0_0_Coeff */

    ke[9] = (-49.349184600139665*(-1. + chi->p1))/chi->p28b9;  /* k5b2PN_et0_2_Coeff */

    ke[10] = (-0.00007176518305897736*(3.635788e6 - 2.258257e6*chi->p1 - 3.883073e6*chi->p19b9 + 2.505542e6*chi->p28b9))/chi->p47b9;  /* k5b2PN_et0_2_Coeff */

    ke[11] = (-5.426889221035796e-9*(-3.3114231189e11 + 1.89286331247e11*chi->p1 + 4.46685572185e11*chi->p19b9 - 2.17621452319e11*chi->p28b9 - 1.61105816843e11*chi->p38b9 + 7.389767762e10*chi->p47b9))/chi->p22b3; /* k5b2PN_et0_6_Coeff */

    /////// k 3PN Coefficients //////
    ke[12] = 0.03125*(2160. - 3978.038658666009*eta + 224.*eta_2);  /* k1PN_et0_0_Coeff */

    ke[13] = (9.841899722852105e-7*(-1.193251e6 + 8.9434977e7*chi->p2b3 + 2.85923842e8*chi->p4b3 - 2.2282512e7*eta - 1.85795232e8*chi->p2b3*eta - 3.5172988134238017e8*chi->p4b3*eta + 4.270056e7*eta_2 + 2.2703856e7*chi->p2b3*eta_2 - 3.424512e6*chi->p4b3*eta_2))/chi->p31b9; /* k3PN_et0_2_Coeff */

    ke[14] = (1.1858853531487498e-11*(-4.759063292165e12 + 2.043730880707e12*chi->p19b9 + 5.626286182461e13*chi->p25b9 - 3.1968879219858e13*chi->p2b3 + 8.6461368353906e13*chi->p31b9 - 3.1736268138496e13*chi->p4b3 + 3.0348860970792e13*eta - 2.7141856655592e13*chi->p19b9*eta - 9.3285988260024e13*chi->p25b9*eta + 5.8318046249232e13*chi->p2b3*eta - 1.0636065400876634e14*chi->p31b9*eta + 2.9600980825294977e13*chi->p4b3*eta - 3.8763744107088e13*eta_2 + 3.5073241576464e13*chi->p19b9*eta_2 + 1.1206302010176e13*chi->p25b9*eta_2 + 7.646159215968e12*chi->p2b3*eta_2 - 1.035548457216e12*chi->p31b9*eta_2 + 2.19411602592e11*chi->p4b3*eta_2))/chi->p50b9; /* k3PN_et0_4 _Coeff */

    ke[15] = (2.5006017062009736e-16*(3.279894679024105e18 - 4.220015697065866e18*chi->p19b9 - 1.7016310487961012e19*chi->p25b9 + 1.0402025549836075e19*chi->p2b3 - 1.096780037851911e19*chi->p31b9 + 4.930394689428237e17*chi->p38b9 + 8.936345368795117e18*chi->p44b9 + 6.636901757399139e18*chi->p4b3 + 9.145059780731944e18*chi->p50b9 - 1.663185954089631e19*eta + 2.169227230953329e19*chi->p19b9*eta + 2.62215950342017e19*chi->p25b9*eta - 1.6414748179030198e19*chi->p2b3*eta + 1.0229862165375343e19*chi->p31b9*eta - 6.093524899450136e18*chi->p38b9*eta - 1.3792229587322483e19*chi->p44b9*eta - 3.280738282899852e18*chi->p4b3*eta - 1.1249816626155364e19*chi->p50b9*eta + 1.920250379150708e19*eta_2 - 2.3329096412641116e19*chi->p19b9*eta_2 + 3.477817715529865e18*chi->p25b9*eta_2 - 7.473844435281261e18*chi->p2b3*eta_2 + 7.582689456297446e16*chi->p31b9*eta_2 + 7.224047870706816e18*chi->p38b9*eta_2 + 1.6463347202736689e18*chi->p44b9*eta_2 + 6.231184695643471e17*chi->p4b3*eta_2 - 1.0953044957976576e17*chi->p50b9*eta_2))/chi->p23b3; /* k3PN_et0_6_Coeff */


    //////////////////////////////////
    /////// psi 0PN Coefficients //////
    se[0] = 1.; /* psi0PN_et0_0_Coeff */

    se[1] = -1.6108071135430917/chi->p19b9; /* psi0PN_et0_2_Coeff */ 

    se[2] = -5.869201796385629/chi->p19b9 + 5.228286070090015/chi->p38b9; /* psi0PN_et0_4_Coeff */

    se[3] = -22.650035647987842/chi->p19b3 - 13.090170525346323/chi->p19b9 + 38.09998818181824/chi->p38b9; /* psi0PN_et0_6_Coeff */

    /////// psi 3b2PN Coefficients //////
    se[4] = -50.26548245743669; /* psi3b2PN_et0_0_Coeff */

    se[5] = 50.48185195147069/chi->p19b9 - 26.49733920048539/chi->p28b9; /* psi3b2PN_et0_2_Coeff */

    se[6] = 183.93771275738717/chi->p19b9 - 149.62757460393453/chi->p28b9 - 191.14321147088643/chi->p38b9 + 172.00776961014063/chi->p47b9; /* psi3b2PN_et0_4_Coeff */

    se[7] = 989.3998026374857/chi->p19b3 + 410.23909375873296/chi->p19b9 - 1117.760790392296/chi->p22b3 - 469.44472324492716/chi->p28b9 - 1392.9142362231482/chi->p38b9 + 1598.0435005233612/chi->p47b9; /* psi3b2PN_et0_6_Coeff */
    
    se[8] = - 3.4193121693121693 + 6.111111111111111*eta; /* psi1PN_et0_0_Coeff */

    se[9] = 4.6174595113029495/chi->p19b9 - 4.5271989609797405/chi->p25b9 - (10.325370012870012*eta)/chi->p19b9 + (8.814694482444141*eta)/chi->p25b9; /* psi1PN_et0_2_Coeff */

    se[10] = 16.824361793925114/chi->p19b9 - 34.31687792891888/chi->p25b9 - 18.56779383764494/chi->p38b9 + 29.388361977311536/chi->p44b9 - (37.62193481663054*eta)/chi->p19b9 + (52.42443185454516*eta)/chi->p25b9 + (42.454791781926005*eta)/chi->p38b9 - (57.22068643376294*eta)/chi->p44b9; /* psi1PN_et0_4_Coeff */

    se[11] = 98.90333052317057/chi->p19b3 + 37.523631407293855/chi->p19b9 - 114.93378626281779/chi->p25b9 - 135.30872570722394/chi->p38b9 + 329.84901303972686/chi->p44b9 - 190.9748541391356/chi->p7 - (226.81999104715237*eta)/chi->p19b3 - (83.90877658124401*eta)/chi->p19b9 + (162.40199701355033*eta)/chi->p25b9 + (309.37998484943006*eta)/chi->p38b9 - (548.8053794709137*eta)/chi->p44b9 + (371.8380852211337*eta)/chi->p7; /* psi1PN_et0_6_Coeff */

    /////// psi 2PN Coefficients //////
    se[12] = - 96.10716450932226 + 43.85912698412698*eta + 42.84722222222222*eta_2; /* psi2PN_et0_0_Coeff */

    se[13] = 56.34521097966795/chi->p19b9 + 12.977443249525058/chi->p25b9 + 0.6305695963516759/chi->p31b9 - (11.02099990468567*eta)/chi->p19b9 - (54.287380863896644*eta)/chi->p25b9 + (11.775120739510275*eta)/chi->p31b9 - (36.277272048441404*eta_2)/chi->p19b9 + (56.50271923709424*eta_2)/chi->p25b9 - (22.564971563560825*eta_2)/chi->p31b9; /* psi2PN_et0_2_Coeff */

    se[14] = 205.30168430420673/chi->p19b9 + 98.37105452220848/chi->p25b9 - 13.013337592397109/chi->p31b9 - 210.57745782683898/chi->p38b9 - 104.3701586151748/chi->p44b9 + 37.20487942148135/chi->p50b9 - (40.1565599597264*eta)/chi->p19b9 - (370.2505090644721*eta)/chi->p25b9 + (172.82419465202892*eta)/chi->p31b9 - (8.268968981387623*eta)/chi->p38b9 + (441.85392048937666*eta)/chi->p44b9 - (237.25797361353315*eta)/chi->p50b9 - (132.18133225545043*eta_2)/chi->p19b9 + (336.04374605848017*eta_2)/chi->p25b9 - (223.32682712918256*eta_2)/chi->p31b9 + (179.853147826764*eta_2)/chi->p38b9 - (464.6441100577457*eta_2)/chi->p44b9 + (303.0429176690523*eta_2)/chi->p50b9; /* psi2PN_et0_4_Coeff */

    se[15] = 1106.8518202223822/chi->p19b3 + 457.88748622305997/chi->p19b9 - 510.1379816462184/chi->p23b3 + 329.46347212360456/chi->p25b9 - 66.1985526505701/chi->p31b9 - 1534.5370446460222/chi->p38b9 - 1171.4294875159458/chi->p44b9 + 695.655631413319/chi->p50b9 + 833.9081409885185/chi->p7 + (279.06106249767885*eta)/chi->p19b3 - (89.56178980041588*eta)/chi->p19b9 + (2586.8340564340087*eta)/chi->p23b3 - (1202.2659321691117*eta)/chi->p25b9 + (818.1546393207492*eta)/chi->p31b9 - (60.25829808147164*eta)/chi->p38b9 + (4627.477530254056*eta)/chi->p44b9 - (3575.8993505095737*eta)/chi->p50b9 - (3536.1065648880704*eta)/chi->p7 - (1167.375190010506*eta_2)/chi->p19b3 - (294.80604680466854*eta_2)/chi->p19b9 - (2986.6588672499524*eta_2)/chi->p23b3 + (1041.0065214484537*eta_2)/chi->p25b9 - (969.9457009894719*eta_2)/chi->p31b9 + (1310.6403733077561*eta_2)/chi->p38b9 - (4456.416080124205*eta_2)/chi->p44b9 + (3845.7243906751146*eta_2)/chi->p50b9 + (3723.628186357419*eta_2)/chi->p7; /* psi2PN_et0_6_Coeff */

    /////// psi 5b2PN Coefficients //////
    se[16] = 60.06010399779534 - 22.689280275926283*eta; /* psi5b2PN_et0_0_Coeff */

    se[17] = - 210.61264554948588/chi->p19b9 + 141.88004620884573/chi->p25b9 + 75.95595393565333/chi->p28b9 - 42.66622732728404/chi->p34b9 + (216.02141641228727*eta)/chi->p19b9 - (276.2479120677702*eta)/chi->p25b9 - (169.84953027662291*eta)/chi->p28b9 + (211.84863109153514*eta)/chi->p34b9; /* psi5b2PN_et0_2_Coeff */

    se[18] = - 767.3967337291026/chi->p19b9 + 1075.4729951706481/chi->p25b9 + 428.91495927680893/chi->p28b9 - 1114.0818153683329/chi->p34b9 + 1381.167270248381/chi->p38b9 - 1074.422059716312/chi->p44b9 - 610.8703238075171/chi->p47b9 + 760.3988519534003/chi->p53b9 + (787.1043494934547*eta)/chi->p19b9 - (1642.9542589366743*eta)/chi->p25b9 - (959.1217091881848*eta)/chi->p28b9 + (1836.068203045879*eta)/chi->p34b9 - (1226.808302894472*eta)/chi->p38b9 + (2091.9562588758126*eta)/chi->p44b9 + (1396.7395712045059*eta)/chi->p47b9 - (2316.4824640732704*eta)/chi->p53b9; /* psi5b2PN_et0_4_Coeff */

    se[19] = - 8927.106385535517/chi->p19b3 - 1711.5366711865092/chi->p19b9 + 4880.7987155565725/chi->p22b3 + 3601.964712944092/chi->p25b9 + 1345.6868821558517/chi->p28b9 - 6026.136596175849/chi->p34b9 + 10064.953594375815/chi->p38b9 - 12059.095238419155/chi->p44b9 - 5675.30962604641/chi->p47b9 + 14635.606205092497/chi->p53b9 + 8342.171550214278/chi->p7 - 8082.796244251813/chi->p8 + (8387.957942353512*eta)/chi->p19b3 + (1755.4908680182211*eta)/chi->p19b9 - (11193.38160918795*eta)/chi->p22b3 - (5089.593596236571*eta)/chi->p25b9 - (3009.168774671892*eta)/chi->p28b9 + (7786.856910831304*eta)/chi->p34b9 - (8940.096470434935*eta)/chi->p38b9 + (20064.017404228016*eta)/chi->p44b9 + (12976.452161121204*eta)/chi->p47b9 - (27842.943893106512*eta)/chi->p53b9 - (16242.646759965388*eta)/chi->p7 + (21169.869809627333*eta)/chi->p8; /* psi5b2PN_et0_6_Coeff */

    /////// psi 3PN Coefficients //////
    se[20] = 229.55467539143297 - 4322.017571041981*eta + 63.80497685185184*eta_2 - 98.6304012345679*eta_3; /* psi3PN_et0_0_Coeff */

    se[21] = - 61.572441713118685/chi->p19b9 + 158.359109826785/chi->p25b9 + 830.4127436366826/chi->p28b9 - 1.8075594251680198/chi->p31b9 - 327.46225593097415/chi->p37b9 - (843.1840289315528*eta)/chi->p19b9 - (339.30821080736393*eta)/chi->p25b9 - (29.71198627806088*eta)/chi->p31b9 + (28.006148883848002*eta)/chi->p37b9 - (92.88746834092433*eta_2)/chi->p19b9 - (41.648488332329705*eta_2)/chi->p25b9 + (140.16285329998192*eta_2)/chi->p31b9 + (7.068382176074206*eta_2)/chi->p37b9 - (92.75010314857681*eta_3)/chi->p19b9 + (198.5172942650821*eta_3)/chi->p25b9 - (144.6428183515857*eta_3)/chi->p31b9 + (35.5644754417582*eta_3)/chi->p37b9 - (14.135045491064645*ln_chi)/chi->p37b9; /* psi3PN_et0_2_Coeff */

    se[22] = - 224.347833127953/chi->p19b9 + 1200.3868810930214/chi->p25b9 + 4689.249883183728/chi->p28b9 + 37.303385945224036/chi->p31b9 - 813.8958832352637/chi->p37b9 + 23.754151159535464/chi->p38b9 - 1183.6625754433232/chi->p44b9 - 6288.507751960206/chi->p47b9 - 132.12982640802795/chi->p50b9 + 3528.9609508334265/chi->p56b9 - (3072.259351030208*eta)/chi->p19b9 - (2068.572924531077*eta)/chi->p25b9 - (578.8255088297584*eta)/chi->p31b9 + (898.2444524268268*eta)/chi->p37b9 + (305.53635467707795*eta)/chi->p38b9 + (2258.1731512868505*eta)/chi->p44b9 + (1144.7121426163849*eta)/chi->p50b9 - (374.23363516095196*eta)/chi->p56b9 - (338.44852773781963*eta_2)/chi->p19b9 - (414.17315108632*eta_2)/chi->p25b9 + (1747.9910011211275*eta_2)/chi->p31b9 - (224.79069843736855*eta_2)/chi->p37b9 + (411.09532675080675*eta_2)/chi->p38b9 + (1101.4595251876121*eta_2)/chi->p44b9 - (3002.815086461273*eta_2)/chi->p50b9 + (784.0895319591057*eta_2)/chi->p56b9 - (337.9480183801763*eta_3)/chi->p19b9 + (1180.659906690582*eta_3)/chi->p25b9 - (1431.5383291528901*eta_3)/chi->p31b9 + (591.9984576931926*eta_3)/chi->p37b9 + (535.9959136507457*eta_3)/chi->p38b9 - (1968.3927845484725*eta_3)/chi->p44b9 + (2460.772765329089*eta_3)/chi->p50b9 - (1032.442463984194*eta_3)/chi->p56b9 + (216.01471299539128*ln_chi)/chi->p37b9 + (91.75780367454276*ln_chi)/chi->p56b9; /* psi3PN_et0_4_Coeff */

    se[23] = - 2127.154767957407/chi->p19b3 - 500.36640322119416/chi->p19b9 + 48826.07350369903/chi->p22b3 + 2227.561412940261/chi->p23b3 - 32553.810341481174/chi->p25b3 + 4020.325202951301/chi->p25b9 + 14712.151951032203/chi->p28b9 + 189.7614767161797/chi->p31b9 + 1799.2380407016244/chi->p37b9 + 173.1031673314792/chi->p38b9 - 13285.188626146024/chi->p44b9 - 58423.5756546798/chi->p47b9 - 2470.5592182443133/chi->p50b9 + 29156.777235788355/chi->p56b9 + 9332.47382943455/chi->p7 + (4500.5028725447555*eta)/chi->p19b3 - (6852.1070152744305*eta)/chi->p19b9 - (16404.21242140086*eta)/chi->p23b3 + (6619.517206182367*eta)/chi->p25b3 - (6467.105803737125*eta)/chi->p25b9 - (2769.61805915347*eta)/chi->p31b9 + (4864.168343934332*eta)/chi->p37b9 + (2226.529181122649*eta)/chi->p38b9 + (21582.317585649853*eta)/chi->p44b9 + (18348.36036551368*eta)/chi->p50b9 - (11603.982243956996*eta)/chi->p56b9 - (15817.900744912906*eta)/chi->p7 - (1707.7368030260425*eta_2)/chi->p19b3 - (754.8469273741383*eta_2)/chi->p19b9 + (38946.34870946779*eta_2)/chi->p23b3 - (16207.47774664614*eta_2)/chi->p25b3 - (1477.3040286445805*eta_2)/chi->p25b9 + (8024.8182867665155*eta_2)/chi->p31b9 - (1807.119638118998*eta_2)/chi->p37b9 + (2995.7670412125676*eta_2)/chi->p38b9 + (12214.794123021338*eta_2)/chi->p44b9 - (42694.80992604206*eta_2)/chi->p50b9 + (17114.129661563496*eta_2)/chi->p56b9 - (14424.031946538573*eta_2)/chi->p7 - (4164.277909396126*eta_3)/chi->p19b3 - (753.7306336993971*eta_3)/chi->p19b9 - (29908.736041689786*eta_3)/chi->p23b3 + (15629.616902071488*eta_3)/chi->p25b3 + (3657.484112987267*eta_3)/chi->p25b9 - (6217.409999562283*eta_3)/chi->p31b9 + (3638.1358466190395*eta_3)/chi->p37b9 + (3905.952677766288*eta_3)/chi->p38b9 - (18878.916287074204*eta_3)/chi->p44b9 + (31228.09771080014*eta_3)/chi->p50b9 - (17384.679703215024*eta_3)/chi->p56b9 + (19164.40936933914*eta_3)/chi->p7 - (596.2711765426641*ln_chi)/chi->p25b3 + (1858.1812925206564*ln_chi)/chi->p37b9 - (1067.9294554602477*ln_chi)/chi->p56b9; /* psi3PN_et0_6_Coeff */

}


////////////////////////////////////////////////////////////////////
// for reference phasing
// function to calculate PN coefficients for psi (fourier phase).
// Harmonic indices dependent
////////////////////////////////////////////////////////////////////
void psi_PNe0Coeff2( REAL8 *se, chi_struct *chi, REAL8 eta, REAL8 ll, REAL8 nn, REAL8 ln_x, REAL8 ln_v3 )
{
    REAL8 eta_2 = eta*eta;
    //////////////////////////////////
    /////// psi 1PN Coefficients //////
    se[8] = -(8.333333333333332*nn)/ll; /* psi1PN_et0_0_Coeff */

    se[9] = (10.494186046511627*nn)/(chi->p19b9*ll); /* psi1PN_et0_2_Coeff */

    se[10] = ((38.23703972868217/chi->p19b9 - 37.50080222848265/chi->p38b9)*nn)/ll; /* psi1PN_et0_4_Coeff */

    se[11] = ((184.58456450704332/chi->p19b3 + 85.28065447351422/chi->p19b9 - 273.2788723799295/chi->p38b9)*nn)/ll; /* psi1PN_et0_6_Coeff */

    /////// psi 2PN Coefficients //////
    se[12] = (( - 126.21031746031746 - 10.*eta)*nn)/ll; /* psi2PN_et0_0_Coeff */

    se[13] = ((64.19878487102324/chi->p19b9 + 29.494076458102615/chi->p25b9 + (28.998243751150728*eta)/chi->p19b9 - (57.42651808785529*eta)/chi->p25b9)*nn)/ll; /* psi2PN_et0_2_Coeff */

    se[14] = ((233.91728303334457/chi->p19b9 + 223.56972383203305/chi->p25b9 - 222.6873092462439/chi->p38b9 - 210.79319982795903/chi->p44b9 + (105.6591710362652*eta)/chi->p19b9 - (341.53793873815675*eta)/chi->p25b9 - (176.47747645649966*eta)/chi->p38b9 + (410.42544661172684*eta)/chi->p44b9)*nn)/ll; /* psi2PN_et0_4_Coeff */

    se[15] = ((1130.9419871413997/chi->p19b3 + 521.7092936926844/chi->p19b9 + 748.7777561517121/chi->p25b9 - 1622.7849311957646/chi->p38b9 - 2365.9001128547707/chi->p44b9 + 1556.3335453823029/chi->p7 + (1233.6668000381603*eta)/chi->p19b3 + (235.65326502884656*eta)/chi->p19b9 - (1058.0265983780876*eta)/chi->p25b9 - (1286.0409084757641*eta)/chi->p38b9 + (3936.4031962986637*eta)/chi->p44b9 - (3030.2632673239614*eta)/chi->p7)*nn)/ll; /* psi2PN_et0_6_Coeff */

    /////// psi 5b2PN Coefficients //////
    se[16] =   - 6.960539278786909*ln_v3 - 22.689280275926283*eta*ln_v3 + (( - 100.53096491487338 - 167.5516081914556*ln_v3)*nn)/ll; /* psi5b2PN_et0_0_Coeff */

    se[17] = (( - 331.6374815048508/chi->p19b9 + 172.62650814583742/chi->p28b9)*nn)/ll; /* psi5b2PN_et0_2_Coeff */

    se[18] = (( - 1208.3677094743632/chi->p19b9 + 974.8029993039772/chi->p28b9 + 1371.759871506524/chi->p38b9 - 1233.7560078844797/chi->p47b9)*nn)/ll; /* psi5b2PN_et0_4_Coeff */

    se[19] = (( - 8112.524009764331/chi->p19b3 - 2695.04098224778/chi->p19b9 + 9109.09774819447/chi->p22b3 + 3058.3675865754967/chi->p28b9 + 9996.399239070568/chi->p38b9 - 11462.24832808487/chi->p47b9)*nn)/ll; /* psi5b2PN_et0_6_Coeff */

    /////// psi 3PN Coefficients //////
    se[20] = -163.04761904761904*ln_x + ((507.8074314216428 - 1013.6970233660937*eta + 19.791666666666664*eta_2)*nn)/ll; /* psi3PN_et0_0_Coeff */

    se[21] = ( - 21.855386904761904/chi->p19b9 + 21.202568236596964/chi->p37b9)*ln_x + ((222.04259821895513/chi->p19b9 + 180.43170390834212/chi->p25b9 - 4.108073898949294/chi->p31b9 - (309.52963269393405*eta)/chi->p19b9 - (269.8099928586847*eta)/chi->p25b9 - (76.71328660124686*eta)/chi->p31b9 + (42.79468040085441*eta_2)/chi->p19b9 - (158.6848338604637*eta_2)/chi->p25b9 + (147.00767567470572*eta_2)/chi->p31b9)*nn)/ll; /* psi3PN_et0_2_Coeff */

    se[22] = ( - 79.63316961022348/chi->p19b9 - 324.0220694930869/chi->p37b9 + 552.5514750417711/chi->p38b9 - 137.6367055118141/chi->p56b9)*ln_x + ((809.0433704841975/chi->p19b9 + 1367.7006049209328/chi->p25b9 + 84.7800985186211/chi->p31b9 - 683.5025421320212/chi->p38b9 - 1251.732434711526/chi->p44b9 - 266.858547220905/chi->p50b9 - (1127.8146594758143*eta)/chi->p19b9 - (1471.5949611373971*eta)/chi->p25b9 - (1125.9257776852457*eta)/chi->p31b9 + (349.71794556861516*eta)/chi->p38b9 + (1445.2033869067816*eta)/chi->p44b9 + (1701.7745827856886*eta)/chi->p50b9 + (155.9284243114465*eta_2)/chi->p19b9 - (943.7607027261389*eta_2)/chi->p25b9 + (1454.9434586961715*eta_2)/chi->p31b9 - (428.7624994075837*eta_2)/chi->p38b9 + (1931.4479367739132*eta_2)/chi->p44b9 - (2173.6286748467505*eta_2)/chi->p50b9)*nn)/ll; /* psi3PN_et0_4_Coeff */

    se[23] = ( - 3566.310930959158/chi->p19b3 - 177.60707602754258/chi->p19b9 + 894.4067648139961/chi->p25b3 - 2787.2719387809843/chi->p37b9 + 4026.5977007978186/chi->p38b9 + 1601.8941831903715/chi->p56b9)*ln_x + ((3342.8217057556644/chi->p19b3 + 1804.421802051675/chi->p19b9 + 4157.326665798624/chi->p23b3 + 4580.690857807921/chi->p25b9 + 431.27443483710056/chi->p31b9 - 4980.874884878742/chi->p38b9 - 14049.19091775737/chi->p44b9 - 4989.712479966107/chi->p50b9 + 9535.591218963053/chi->p7 + (1186.2079528535864*eta)/chi->p19b3 - (2515.382282922241*eta)/chi->p19b9 - (21081.18703121239*eta)/chi->p23b3 - (4403.465107627784*eta)/chi->p25b9 - (5330.164566360338*eta)/chi->p31b9 + (2548.4928357993604*eta)/chi->p38b9 + (12241.310242655041*eta)/chi->p44b9 + (25648.767595096477*eta)/chi->p50b9 - (8164.577251678136*eta)/chi->p7 + (4215.308124947458*eta_2)/chi->p19b3 + (347.76954938614654*eta_2)/chi->p19b9 + (24339.525769859294*eta_2)/chi->p23b3 - (2923.6105648391167*eta_2)/chi->p25b9 + (6319.062385321006*eta_2)/chi->p31b9 - (3124.512687568861*eta_2)/chi->p38b9 + (18524.577105458782*eta_2)/chi->p44b9 - (27584.135195853287*eta_2)/chi->p50b9 - (20252.696633959793*eta_2)/chi->p7)*nn)/ll; /* psi3PN_et0_6_Coeff */

}


#endif