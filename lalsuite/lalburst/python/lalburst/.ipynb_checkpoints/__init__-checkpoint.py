# Import SWIG wrappings, if available
from .lalburst import *

__version__ = "1.5.9.1"

## \addtogroup lalburst_python
"""This package provides Python wrappings and extensions to LALBurst"""
